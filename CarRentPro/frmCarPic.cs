﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using MySql.Data.MySqlClient;
using System.IO;

namespace CarRentPro
{
    public partial class frmCarPic : DevExpress.XtraEditors.XtraForm
    {
        public frmCarPic()
        {
            InitializeComponent();
        }
        public Image ByteArray2Image(byte[] Barry)
        {
            MemoryStream ms = new MemoryStream(Barry);
            Image retImage = Image.FromStream(ms);
            return retImage;
        }
        private void frmCarPic_Load(object sender, EventArgs e)
        {
            using (MySqlConnection con  = new MySqlConnection(AppSett.CS))
                {
                     con.Open();
                     MySqlCommand cmd = new MySqlCommand();
                     cmd.Connection = con;
                     cmd.CommandText =  "SELECT `Image1`, `Image2`, `Image3`,`Image4` FROM `tblvehicle` WHERE `vhclNo` = '" + this.Text+ "'";
                     MySqlDataReader rdr = cmd.ExecuteReader();
                   while (rdr.Read())
                    {
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[0]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[1]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[2]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[3]));
                    }
                }
        }
    }
}