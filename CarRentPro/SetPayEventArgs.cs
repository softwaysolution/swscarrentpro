﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentPro
{
     public static class SetPayEventArgs
    {
        private static double _totAmount;
        private static double _duePayment;

        public static double DuePayment
        {
            get { return _duePayment; }
            set { _duePayment = value; }
        }

        public static double TotAmount
        {
            get { return _totAmount; }
            set { _totAmount = value; }
        }
        
    }
}
