﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;

namespace CarRentPro.Forms
{
    public partial class frmVehicleMaintenance : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand comm;
        public int GetLastID(string Item)
        {
            int lastID = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT " + Item + " FROM tbllastid";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    lastID = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return lastID + 1;
        }
        public void UpdateID(string Item, int NewID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                int intnewid = NewID;
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tbllastid` SET " + Item.Trim() + "='" + intnewid + "' WHERE clmnID=1";
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                comm.Dispose();
            }
        }
        public frmVehicleMaintenance()
        {
            InitializeComponent();
        }

        private void frmVehicleMaintenance_Load(object sender, EventArgs e)
        {
            #region Load Vehicle
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    string qry = "SELECT vhclNo FROM tblvehicle";
                    comm = new MySqlCommand(qry, con);
                    con.Open();
                    MySqlDataReader rdr = comm.ExecuteReader();

                    while (rdr.Read())
                    {
                        txtVhclNo.Properties.Items.Add(rdr[0].ToString());
                    }
                    comm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion
            checkEdit11.Checked = false;
            checkEdit2.Checked = false;
            checkEdit3.Checked = false;
            checkEdit6.Checked = false;
            txtEoilIterval.Enabled = false;
            txtEoilNext.Enabled = false;
            txtEoilNote.Enabled = false;
            txtGoilInterval.Enabled = false;
            txtGoilNext.Enabled = false;
            txtGoilNote.Enabled = false;
            txtOilFilter.Enabled = false;
            txtOtherFilter.Enabled = false;

        }

        private void btnVhclSave_Click(object sender, EventArgs e)
        {
            try
            {
                string NewID = string.Format("MTC{0}", this.GetLastID("`tblMTC`").ToString());
                using (con = new MySqlConnection(AppSett.CS))
                {
                    string qry = "INSERT INTO tblmaintanance (`MrecordID`,`MDate`,`MVhclNo`,`MCurODO`,`MEoilNext`,`MEoilNote`,`MGoilNext`,`MGoilNote`,`MoilFilter`,`MOtherFilter`,`MCharge`) VALUES (@MrecordID,@MDate,@MVhclNo,@MCurODO,@MEoilNext,@MEoilNote,@MGoilNext,@MGoilNote,@MoilFilter,@MOtherFilter,@MCharge)";
                    comm = new MySqlCommand(qry, con);
                    comm.Parameters.AddWithValue("@MrecordID", NewID);
                    comm.Parameters.AddWithValue("@MDate", DateTime.Parse(dtpServiced.EditValue.ToString()));
                    comm.Parameters.AddWithValue("@MVhclNo", txtVhclNo.Text.Trim());
                    comm.Parameters.AddWithValue("@MCurODO", int.Parse(txtCurrentODO.Text.Trim()));
                    comm.Parameters.AddWithValue("@MEoilNext", int.Parse(txtEoilNext.Text.Trim()));
                    comm.Parameters.AddWithValue("@MEoilNote", txtEoilNote.Text.Trim());
                    comm.Parameters.AddWithValue("@MGoilNext", int.Parse(txtGoilNext.Text.Trim()));
                    comm.Parameters.AddWithValue("@MGoilNote", txtGoilNote.Text.Trim());
                    comm.Parameters.AddWithValue("@MoilFilter", txtOilFilter.Text.Trim());
                    comm.Parameters.AddWithValue("@MOtherFilter", txtOtherFilter.Text.Trim());
                    comm.Parameters.AddWithValue("@MCharge", int.Parse(txtMCharges.Text.Trim()));
                    con.Open();
                    comm.ExecuteNonQuery();
                }
                this.UpdateID("tblMTC", this.GetLastID("`tblMTC`"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void checkEdit11_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit11.Checked==false)
            {
                txtEoilIterval.Enabled = false;
                txtEoilNext.Enabled = false;
                txtEoilNote.Enabled = false;
                txtEoilIterval.Text = "";
                txtEoilNext.Text = "";
                txtEoilNote.Text = "";
            }
            else
            {
                txtEoilIterval.Enabled = true;
                txtEoilNext.Enabled = true;
                txtEoilNote.Enabled = true;
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit2.Checked==false)
            {
                txtGoilInterval.Enabled = false;
                txtGoilNext.Enabled = false;
                txtGoilNote.Enabled = false;
                txtGoilInterval.Text = "";
                txtGoilNext.Text = "";
                txtGoilNote.Text = "";
            }
            else
            {
                txtGoilInterval.Enabled = true;
                txtGoilNext.Enabled = true;
                txtGoilNote.Enabled = true;
            }
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit3.Checked==false)
            {
                txtOilFilter.Enabled = false;
                txtOilFilter.Text = "";
            }
            else
            {
                txtOilFilter.Enabled = true;
            }
        }

        private void checkEdit6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit6.Checked==false)
            {
                txtOtherFilter.Enabled = false;
                txtOtherFilter.Text = "";
            }
            else
            {
                txtOtherFilter.Enabled = true;
            }
        }

        private void txtEoilIterval_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                int CurODO;
                int interval;
                if (int.TryParse(txtEoilIterval.Text.Trim(),out interval))
                {
                    if (int.TryParse(txtCurrentODO.Text.Trim(),out CurODO))
                    {
                        txtEoilNext.Text = (CurODO + interval).ToString();
                        txtEoilNote.Focus();
                    }
                }
            }
        }

        private void txtGoilInterval_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int CurODO;
                int interval;
                if (int.TryParse(txtGoilInterval.Text.Trim(),out interval))
                {
                    if (int.TryParse(txtCurrentODO.Text.Trim(), out CurODO))
                    {
                        txtGoilNext.Text= (CurODO + interval).ToString();
                        txtGoilNote.Focus();
                    }
                }
            }
        }
    }
}