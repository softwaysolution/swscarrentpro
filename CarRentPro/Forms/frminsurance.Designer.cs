﻿namespace CarRentPro.Forms
{
    partial class frminsurance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frminsurance));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.digitalGauge3 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge();
            this.digitalBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lstOrderList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtVhclNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclMYeay = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclModel = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclMake = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGauge3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(895, 544);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gaugeControl1);
            this.xtraTabPage1.Controls.Add(this.simpleButton6);
            this.xtraTabPage1.Controls.Add(this.simpleButton5);
            this.xtraTabPage1.Controls.Add(this.simpleButton4);
            this.xtraTabPage1.Controls.Add(this.simpleButton3);
            this.xtraTabPage1.Controls.Add(this.simpleButton1);
            this.xtraTabPage1.Controls.Add(this.simpleButton2);
            this.xtraTabPage1.Controls.Add(this.pictureEdit7);
            this.xtraTabPage1.Controls.Add(this.pictureEdit3);
            this.xtraTabPage1.Controls.Add(this.pictureEdit6);
            this.xtraTabPage1.Controls.Add(this.pictureEdit5);
            this.xtraTabPage1.Controls.Add(this.pictureEdit2);
            this.xtraTabPage1.Controls.Add(this.pictureEdit1);
            this.xtraTabPage1.Controls.Add(this.labelControl3);
            this.xtraTabPage1.Controls.Add(this.textEdit3);
            this.xtraTabPage1.Controls.Add(this.labelControl2);
            this.xtraTabPage1.Controls.Add(this.textEdit2);
            this.xtraTabPage1.Controls.Add(this.labelControl1);
            this.xtraTabPage1.Controls.Add(this.textEdit1);
            this.xtraTabPage1.Controls.Add(this.panel3);
            this.xtraTabPage1.Controls.Add(this.labelControl11);
            this.xtraTabPage1.Controls.Add(this.labelControl10);
            this.xtraTabPage1.Controls.Add(this.labelControl6);
            this.xtraTabPage1.Controls.Add(this.labelControl9);
            this.xtraTabPage1.Controls.Add(this.txtVhclMYeay);
            this.xtraTabPage1.Controls.Add(this.txtVhclModel);
            this.xtraTabPage1.Controls.Add(this.txtVhclMake);
            this.xtraTabPage1.Controls.Add(this.txtVhclType);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(795, 538);
            this.xtraTabPage1.Text = "Vehicle Deatils";
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.digitalGauge3});
            this.gaugeControl1.Location = new System.Drawing.Point(578, 35);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(126, 49);
            this.gaugeControl1.TabIndex = 56;
            // 
            // digitalGauge3
            // 
            this.digitalGauge3.AppearanceOff.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#0D8097");
            this.digitalGauge3.AppearanceOn.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#02F0F7");
            this.digitalGauge3.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent[] {
            this.digitalBackgroundLayerComponent1});
            this.digitalGauge3.Bounds = new System.Drawing.Rectangle(6, 6, 114, 37);
            this.digitalGauge3.DigitCount = 5;
            this.digitalGauge3.Name = "digitalGauge3";
            this.digitalGauge3.Padding = new DevExpress.XtraGauges.Core.Base.TextSpacing(26, 20, 26, 20);
            this.digitalGauge3.Text = "00.000";
            // 
            // digitalBackgroundLayerComponent1
            // 
            this.digitalBackgroundLayerComponent1.BottomRight = new DevExpress.XtraGauges.Core.Base.PointF2D(265.8125F, 99.9625F);
            this.digitalBackgroundLayerComponent1.Name = "digitalBackgroundLayerComponent1";
            this.digitalBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.DigitalBackgroundShapeSetType.Style17;
            this.digitalBackgroundLayerComponent1.TopLeft = new DevExpress.XtraGauges.Core.Base.PointF2D(26F, 0F);
            this.digitalBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(760, 393);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(36, 22);
            this.simpleButton6.TabIndex = 55;
            this.simpleButton6.Text = "Set";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(760, 233);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(36, 22);
            this.simpleButton5.TabIndex = 55;
            this.simpleButton5.Text = "Set";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(578, 393);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(36, 22);
            this.simpleButton4.TabIndex = 55;
            this.simpleButton4.Text = "Set";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(578, 230);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(36, 22);
            this.simpleButton3.TabIndex = 55;
            this.simpleButton3.Text = "Set";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(392, 393);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(36, 22);
            this.simpleButton1.TabIndex = 55;
            this.simpleButton1.Text = "Set";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(393, 233);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(36, 22);
            this.simpleButton2.TabIndex = 55;
            this.simpleButton2.Text = "Set";
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit7.EditValue = global::CarRentPro.Properties.Resources.nic;
            this.pictureEdit7.Location = new System.Drawing.Point(616, 393);
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit7.Properties.InitialImage")));
            this.pictureEdit7.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit7.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit7.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit7.Size = new System.Drawing.Size(144, 140);
            this.pictureEdit7.TabIndex = 54;
            this.pictureEdit7.EditValueChanged += new System.EventHandler(this.pictureEdit7_EditValueChanged);
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::CarRentPro.Properties.Resources.nic;
            this.pictureEdit3.Location = new System.Drawing.Point(617, 230);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit3.Properties.InitialImage")));
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit3.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit3.Size = new System.Drawing.Size(144, 140);
            this.pictureEdit3.TabIndex = 54;
            this.pictureEdit3.EditValueChanged += new System.EventHandler(this.pictureEdit3_EditValueChanged);
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit6.EditValue = global::CarRentPro.Properties.Resources.nic;
            this.pictureEdit6.Location = new System.Drawing.Point(431, 393);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit6.Properties.InitialImage")));
            this.pictureEdit6.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit6.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit6.Size = new System.Drawing.Size(144, 140);
            this.pictureEdit6.TabIndex = 54;
            this.pictureEdit6.EditValueChanged += new System.EventHandler(this.pictureEdit6_EditValueChanged);
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit5.EditValue = global::CarRentPro.Properties.Resources.nic;
            this.pictureEdit5.Location = new System.Drawing.Point(248, 393);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit5.Properties.InitialImage")));
            this.pictureEdit5.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit5.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit5.Size = new System.Drawing.Size(144, 140);
            this.pictureEdit5.TabIndex = 54;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::CarRentPro.Properties.Resources.nic;
            this.pictureEdit2.Location = new System.Drawing.Point(431, 230);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit2.Properties.InitialImage")));
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit2.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit2.Size = new System.Drawing.Size(144, 140);
            this.pictureEdit2.TabIndex = 54;
            this.pictureEdit2.EditValueChanged += new System.EventHandler(this.pictureEdit2_EditValueChanged);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::CarRentPro.Properties.Resources.nic;
            this.pictureEdit1.Location = new System.Drawing.Point(248, 230);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit1.Properties.InitialImage")));
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.pictureEdit1.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit1.Size = new System.Drawing.Size(144, 140);
            this.pictureEdit1.TabIndex = 54;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(262, 210);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(38, 13);
            this.labelControl3.TabIndex = 40;
            this.labelControl3.Text = "Colour :";
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(327, 207);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(193, 20);
            this.textEdit3.TabIndex = 41;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(262, 67);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(59, 13);
            this.labelControl2.TabIndex = 38;
            this.labelControl2.Text = "Chassis No :";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(327, 64);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(193, 20);
            this.textEdit2.TabIndex = 39;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(262, 41);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(56, 13);
            this.labelControl1.TabIndex = 36;
            this.labelControl1.Text = "Vehicle No :";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(327, 38);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(193, 20);
            this.textEdit1.TabIndex = 37;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureEdit4);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.txtVhclNo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(242, 538);
            this.panel3.TabIndex = 35;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(202, 7);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit4.Properties.InitialImage")));
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit4.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit4.Size = new System.Drawing.Size(32, 33);
            this.pictureEdit4.TabIndex = 53;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lstOrderList);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 68);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(242, 470);
            this.panel4.TabIndex = 0;
            // 
            // lstOrderList
            // 
            this.lstOrderList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader11,
            this.columnHeader2});
            this.lstOrderList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstOrderList.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstOrderList.FullRowSelect = true;
            this.lstOrderList.Location = new System.Drawing.Point(0, 0);
            this.lstOrderList.MultiSelect = false;
            this.lstOrderList.Name = "lstOrderList";
            this.lstOrderList.Size = new System.Drawing.Size(242, 470);
            this.lstOrderList.TabIndex = 34;
            this.lstOrderList.UseCompatibleStateImageBehavior = false;
            this.lstOrderList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 0;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Vehicle No";
            this.columnHeader11.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Type";
            this.columnHeader2.Width = 100;
            // 
            // txtVhclNo
            // 
            this.txtVhclNo.Location = new System.Drawing.Point(3, 15);
            this.txtVhclNo.Name = "txtVhclNo";
            this.txtVhclNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNo.TabIndex = 30;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(262, 133);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(31, 13);
            this.labelControl11.TabIndex = 32;
            this.labelControl11.Text = "Type :";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(262, 107);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(32, 13);
            this.labelControl10.TabIndex = 25;
            this.labelControl10.Text = "Make :";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(262, 158);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(35, 13);
            this.labelControl6.TabIndex = 24;
            this.labelControl6.Text = "Model :";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(262, 184);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(29, 13);
            this.labelControl9.TabIndex = 23;
            this.labelControl9.Text = "Year :";
            // 
            // txtVhclMYeay
            // 
            this.txtVhclMYeay.Location = new System.Drawing.Point(327, 181);
            this.txtVhclMYeay.Name = "txtVhclMYeay";
            this.txtVhclMYeay.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMYeay.TabIndex = 27;
            // 
            // txtVhclModel
            // 
            this.txtVhclModel.Location = new System.Drawing.Point(327, 155);
            this.txtVhclModel.Name = "txtVhclModel";
            this.txtVhclModel.Size = new System.Drawing.Size(193, 20);
            this.txtVhclModel.TabIndex = 28;
            // 
            // txtVhclMake
            // 
            this.txtVhclMake.Location = new System.Drawing.Point(327, 104);
            this.txtVhclMake.Name = "txtVhclMake";
            this.txtVhclMake.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMake.TabIndex = 29;
            // 
            // txtVhclType
            // 
            this.txtVhclType.Location = new System.Drawing.Point(327, 130);
            this.txtVhclType.Name = "txtVhclType";
            this.txtVhclType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtVhclType.Properties.Items.AddRange(new object[] {
            "Car",
            "Van",
            "Bike"});
            this.txtVhclType.Size = new System.Drawing.Size(193, 20);
            this.txtVhclType.TabIndex = 33;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.labelControl4);
            this.xtraTabPage2.Controls.Add(this.textEdit4);
            this.xtraTabPage2.Controls.Add(this.labelControl5);
            this.xtraTabPage2.Controls.Add(this.textEdit5);
            this.xtraTabPage2.Controls.Add(this.labelControl7);
            this.xtraTabPage2.Controls.Add(this.textEdit6);
            this.xtraTabPage2.Controls.Add(this.labelControl8);
            this.xtraTabPage2.Controls.Add(this.labelControl12);
            this.xtraTabPage2.Controls.Add(this.labelControl13);
            this.xtraTabPage2.Controls.Add(this.labelControl14);
            this.xtraTabPage2.Controls.Add(this.textEdit7);
            this.xtraTabPage2.Controls.Add(this.textEdit8);
            this.xtraTabPage2.Controls.Add(this.textEdit9);
            this.xtraTabPage2.Controls.Add(this.comboBoxEdit1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(795, 538);
            this.xtraTabPage2.Text = "Policy Infromatin";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(90, 233);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(38, 13);
            this.labelControl4.TabIndex = 54;
            this.labelControl4.Text = "Colour :";
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(155, 230);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(193, 20);
            this.textEdit4.TabIndex = 55;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(90, 90);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(59, 13);
            this.labelControl5.TabIndex = 52;
            this.labelControl5.Text = "Chassis No :";
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(155, 87);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(193, 20);
            this.textEdit5.TabIndex = 53;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(90, 64);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(56, 13);
            this.labelControl7.TabIndex = 50;
            this.labelControl7.Text = "Vehicle No :";
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(155, 61);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(193, 20);
            this.textEdit6.TabIndex = 51;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(90, 156);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(31, 13);
            this.labelControl8.TabIndex = 48;
            this.labelControl8.Text = "Type :";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(90, 130);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(32, 13);
            this.labelControl12.TabIndex = 44;
            this.labelControl12.Text = "Make :";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(90, 181);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(35, 13);
            this.labelControl13.TabIndex = 43;
            this.labelControl13.Text = "Model :";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(90, 207);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(29, 13);
            this.labelControl14.TabIndex = 42;
            this.labelControl14.Text = "Year :";
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(155, 204);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(193, 20);
            this.textEdit7.TabIndex = 45;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(155, 178);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(193, 20);
            this.textEdit8.TabIndex = 46;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(155, 127);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(193, 20);
            this.textEdit9.TabIndex = 47;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(155, 153);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.Items.AddRange(new object[] {
            "Car",
            "Van",
            "Bike"});
            this.comboBoxEdit1.Size = new System.Drawing.Size(193, 20);
            this.comboBoxEdit1.TabIndex = 49;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(895, 595);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.xtraTabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(895, 544);
            this.panel2.TabIndex = 1;
            // 
            // frminsurance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 595);
            this.Controls.Add(this.panel1);
            this.Name = "frminsurance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frminsurance";
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.digitalGauge3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.digitalBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtVhclNo;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtVhclMYeay;
        private DevExpress.XtraEditors.TextEdit txtVhclModel;
        private DevExpress.XtraEditors.TextEdit txtVhclMake;
        private DevExpress.XtraEditors.ComboBoxEdit txtVhclType;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ListView lstOrderList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalGauge digitalGauge3;
        private DevExpress.XtraGauges.Win.Gauges.Digital.DigitalBackgroundLayerComponent digitalBackgroundLayerComponent1;
    }
}