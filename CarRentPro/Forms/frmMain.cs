﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;
using MySql.Data.MySqlClient;
using DevExpress.XtraTreeList;
using DevExpress.XtraScheduler.Drawing;

namespace CarRentPro.Forms
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        
        public frmMain()
        {
            InitializeComponent();
        }

        public string VHNo;
        public int AppomentID;
        private void LoadData()
        {
            // TODO: This line of code loads data into the 'carrentproDataSet.taskdependencies' table. You can move, or remove it, as needed.
            this.taskdependenciesTableAdapter.Fill(this.carrentproDataSet.taskdependencies);
            // TODO: This line of code loads data into the 'carrentproDataSet.resources' table. You can move, or remove it, as needed.
            this.resourcesTableAdapter.Fill(this.carrentproDataSet.resources);
            // TODO: This line of code loads data into the 'carrentproDataSet.appointments' table. You can move, or remove it, as needed.
            this.appointmentsTableAdapter.Fill(this.carrentproDataSet.appointments);
            // TODO: This line of code loads data into the 'carrentproDataSet.taskdependencies' table. You can move, or remove it, as needed.
            this.taskdependenciesTableAdapter.Fill(this.carrentproDataSet.taskdependencies);
            // TODO: This line of code loads data into the 'carrentproDataSet.resources' table. You can move, or remove it, as needed.
            this.resourcesTableAdapter.Fill(this.carrentproDataSet.resources);
            // TODO: This line of code loads data into the 'carrentproDataSet.appointments' table. You can move, or remove it, as needed.
            this.appointmentsTableAdapter.Fill(this.carrentproDataSet.appointments);

            schedulerStorage1.Appointments.CommitIdToDataSource = false;
            this.appointmentsTableAdapter.Adapter.RowUpdated += new MySqlRowUpdatedEventHandler(appointmentsTableAdapter_RowUpdated);


        }
        private void schedulerStorage1_AppointmentsChanged(object sender, PersistentObjectsEventArgs e)
        {
            CommitTask();
        }

        private void schedulerStorage1_AppointmentsDeleted(object sender, PersistentObjectsEventArgs e)
        {
            CommitTask();
        }
        private void schedulerStorage1_AppointmentsInserted(object sender, PersistentObjectsEventArgs e)
        {

            CommitTask();
            schedulerStorage1.SetAppointmentId(((Appointment)e.Objects[0]), id);
        }
        void CommitTask()
        {

            appointmentsTableAdapter.Update(carrentproDataSet);
            this.carrentproDataSet.AcceptChanges();
        }

        private void schedulerStorage1_AppointmentDependenciesChanged(object sender, PersistentObjectsEventArgs e)
        {
            CommitTaskDependency();
        }
        int id = 0;
        private void appointmentsTableAdapter_RowUpdated(object sender, MySqlRowUpdatedEventArgs e)
        {
            if (e.Status == UpdateStatus.Continue && e.StatementType == StatementType.Insert)
            {
                id = 0;
                using (MySqlCommand cmd = new MySqlCommand("SELECT @@IDENTITY", appointmentsTableAdapter.Connection))
                {
                    id = Convert.ToInt32(cmd.ExecuteScalar());
                    e.Row["UniqeID"] = id;
                }
            }
        }



        private void schedulerStorage1_AppointmentDependenciesDeleted(object sender, PersistentObjectsEventArgs e)
        {
            CommitTaskDependency();
        }

        private void schedulerStorage1_AppointmentDependenciesInserted(object sender, PersistentObjectsEventArgs e)
        {
            CommitTaskDependency();
        }
        void CommitTaskDependency()
        {
            taskdependenciesTableAdapter.Update(this.carrentproDataSet);
            this.carrentproDataSet.AcceptChanges();
        }

        void navBarControl_ActiveGroupChanged(object sender, DevExpress.XtraNavBar.NavBarGroupEventArgs e)
        {
            navigationFrame.SelectedPageIndex = navBarControl.Groups.IndexOf(e.Group);
        }
        void barButtonNavigation_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int barItemIndex = barSubItemNavigation.ItemLinks.IndexOf(e.Link);
            navBarControl.ActiveGroup = navBarControl.Groups[barItemIndex];
        }

        private void XtraForm1_Load(object sender, EventArgs e)
        {
           // LoadData();
        }
        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCustomer frm = new frmCustomer();
            frm.Show();
        }
        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmVehicles frm = new FrmVehicles();
            frm.Show();
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCheckOut frm = new frmCheckOut();
            frm.Show();
        }

        private void frmMain_Activated(object sender, EventArgs e)
        {
            LoadData();
        }


        private void resourcesTree1_DoubleClick(object sender, EventArgs e)
        {
            TreeListHitInfo hi = resourcesTree1.CalcHitInfo(resourcesTree1.PointToClient(Form.MousePosition));
            if(hi.Node != null)
            {
               // MessageBox.Show(hi.Node.GetValue("Description").ToString());

                frmCarPic frm = new frmCarPic();
                frm.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);
                frm.Text =hi. Node.GetValue("Description").ToString();
                frm.Show();
                   
            }
        }

        private void barButtonItem11_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmVehicleMaintenance frm = new frmVehicleMaintenance();
            frm.Show();
        }

        private void barButtonItem22_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmAddDrivers frm = new frmAddDrivers();
            frm.Show();
        }

        private void barButtonItem12_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCheckIn frm = new frmCheckIn();
            frm.Show();
        }

        private void barButtonItem20_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frminsurance frm = new frminsurance();
            frm.Show();
        }

        private void barButtonItem24_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCompanyDeatils frm = new frmCompanyDeatils();
            frm.Show();
        }

        private void barButtonItem25_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frmCheckout2 frm = new frmCheckout2();
            frm.Show();
        }

        private void ribbonControl_Click(object sender, EventArgs e)
        {

        }
       
        private void schedulerControl1_Click(object sender, EventArgs e)
        {
            Appointment selectedApt;
            if (this.schedulerControl1.SelectedAppointments.Count == 1)
            {
                selectedApt = this.schedulerControl1.SelectedAppointments[0];
                DataRowView row = (DataRowView)selectedApt.GetSourceObject(this.schedulerStorage1);
                AppomentID = Convert.ToInt32(row["UniqeID"]);
                VHNo = row["Description"].ToString();
                frmShowRecord frm = new frmShowRecord();
                frm.Text = AppomentID + "-" + VHNo;
                frm.txtVehID.Text = VHNo.ToString();
                frm.txtCheID.Text = AppomentID.ToString();
                frm.ShowDialog();

            }
          
        }

        private void schedulerControl1_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
           /* DevExpress.XtraScheduler.SchedulerControl scheduler = ((DevExpress.XtraScheduler.SchedulerControl)(sender));
            CarRentPro.Forms.OutlookAppointmentForm form = new CarRentPro.Forms.OutlookAppointmentForm(scheduler, e.Appointment, e.OpenRecurrenceForm);
            
            try
            {
                e.DialogResult = form.ShowDialog();
                e.Handled = true;
                
            }
            finally
            {
                form.Dispose();
                
               
            }*/

        }
        
        private void schedulerControl1_SelectionChanged(object sender, EventArgs e)
        {
            
        }

    }
}