﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.IO;
using DevExpress.XtraGauges.Core.Primitive;
using DevExpress.XtraGauges.Base;
using DevExpress.XtraGauges.Core.Model;
using DevExpress.XtraGauges.Core.Base;

namespace CarRentPro.Forms
{
    public partial class frmCheckOut : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand comm;

        public void loadVhclDetails(string VhclNo)
        {
            #region Load Text Boxes
            this.ClrVhcl();
            string slctID = VhclNo;
            imageSlider1.Images.Clear();
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {

                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `vhclsupID`,`vhclNo`,`vhclMake`,`vhclType`,`vchlModel`,`vhclDelDate`,`vchlOdoReading`,`vchlNextService`,`vhclInsNo`,`vhclInsExpDate`,`vhclLicNo`,`vchlLicExpDate`,`vhclMadeYear`,`vchlId`,`Image1`, `Image2`, `Image3`,`Image4` FROM `tblvehicle` WHERE `vhclNo` = '" + slctID + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        lblVhclID.Text = rdr[13].ToString();
                        txtVhclNo.Text = rdr[1].ToString();
                        txtVhclMake.Text = rdr[2].ToString();
                        txtVhclType.Text = rdr[3].ToString();
                        txtVhclModel.Text = rdr[4].ToString();
                        dtpDelDate.Text = rdr[5].ToString();
                        //picVhcl.Image = this.ByteArray2Image((byte[])rdr[6]);
                        txtVhclCurOdo.Text = rdr[6].ToString();
                        txtVhclNxtSrvice.Text = rdr[7].ToString();
                        txtVhclInsNo.Text = rdr[8].ToString();
                        dtpInsExpDate.Text = rdr[9].ToString();
                        txtVhclLicNo.Text = rdr[10].ToString();
                        dtpLicExpDate.Text = rdr[11].ToString();
                        txtVhclMYeay.Text = rdr[12].ToString();

                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[14]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[15]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[16]));
                        imageSlider1.Images.Add(this.ByteArray2Image((byte[])rdr[17]));



                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }
            }
            #endregion
        }
        public TimeSpan CountTime()
        {
            DateTime strt = dtpStart.Value.AddHours(tpStart.Time.Hour);
            DateTime end = dtpEnd.Value.AddHours(tpEnd.Time.Hour);
            TimeSpan time = end - strt;
            return time;
            
        }
        public int GetResourceID(string Vno)
        {
            int id = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT `Id` FROM `resources` WHERE `Description`='" + Vno + "'";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    id = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return id;
        }
        public int GetLastID(string Item)
        {
            int lastID = 0;
            using (con = new MySqlConnection(AppSett.CS))
            {
                string Qry = "SELECT " + Item + " FROM tbllastid";
                comm = new MySqlCommand(Qry, con);
                con.Open();
                MySqlDataReader rdr = comm.ExecuteReader();

                while (rdr.Read())
                {
                    lastID = int.Parse(rdr[0].ToString());
                }
                comm.Dispose();
            }
            return lastID + 1;
        }
        public void UpdateID(string Item, int NewID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                int intnewid = NewID;
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "UPDATE `tbllastid` SET " + Item.Trim() + "='" + intnewid + "' WHERE clmnID=1";
                    comm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                comm.Dispose();
            }
        }
        public string calPaid()
        {
            double totPaid = 0;

            foreach (ListViewItem item in lstPayments.Items)
            {
                totPaid = totPaid + double.Parse(item.SubItems[4].Text.ToString());
            }
            return AppSett.FormatCurrency(totPaid.ToString());

        }
        public string CalDuePay()
        {
            double Gtot = double.Parse(lblGrandTotal.Text);
            double paid = double.Parse(lblPaid.Text);
            double DuePay = Gtot - paid;
            return AppSett.FormatCurrency(DuePay.ToString());
        }
        public string CalTotal()
        {
            double days;
            double rate;
            string totalcal = "0.00";
            if (double.TryParse(txtDays.Text.Trim(), out days) && double.TryParse(txtDayRate.Text.Trim(), out rate))
            {
                try
                {
                    string total = (days * rate).ToString();
                    totalcal = AppSett.FormatCurrency(total);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return totalcal;
        }
        public frmCheckOut()
        {
            InitializeComponent();
        }
        public Image ByteArray2Image(byte[] Barry)
        {
            MemoryStream ms = new MemoryStream(Barry);
            Image retImage = Image.FromStream(ms);
            return retImage;
        }
        public void loadCustDetails(string CustNic)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                try
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT `ID`,`NIC`,`FName`,`Address1`,`Address2`,`LicNo`,`ExDate`,`Contact1`,`Contact2`,`Contact3`,`Pic`,`BlackLIsted`,`picID`,`picDLic` FROM `tblcustomer` WHERE `NIC` = '" + CustNic + "'";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        lblCstID.Text = rdr[0].ToString();
                        txtNIC.Text = rdr[1].ToString();
                        txtFName.Text = rdr[2].ToString();
                        txtAddress1.Text = rdr[3].ToString();
                        txtAddress2.Text = rdr[4].ToString();
                        txtLIcNo.Text = rdr[5].ToString();
                        dtpLicExp.Text = rdr[6].ToString();
                        txtContact1.Text = rdr[7].ToString();
                        txtContact2.Text = rdr[8].ToString();
                        txtContact3.Text = rdr[9].ToString();
                        PicCustomer.Image = this.ByteArray2Image((byte[])rdr[10]);
                        if ((bool)rdr[11])
                        {
                            lblIsBlackListed.Text = "Black Listed";
                        }
                        else
                        {
                            lblIsBlackListed.Text = "";
                        }
                        //if (rdr[12].ToString() != "")
                        //{
                        //    picNIC.Image = this.ByteArray2Image((byte[])rdr[12]);

                        //}
                        //if (rdr[12].ToString() != "")
                        //{
                        //    picDLic.Image = this.ByteArray2Image((byte[])rdr[13]);

                        //}
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }

            }
        }
        private void ClrVhcl()
        {
            txtVhclNo.Text = "";
            txtVhclMake.Text = "";
            txtVhclType.Text = "";
            txtVhclModel.Text = "";
            dtpDelDate.Text = "";
            txtVhclNxtSrvice.Text = "";
            txtVhclInsNo.Text = "";
            txtVhclLicNo.Text = "";
            txtVhclMYeay.Text = "";
        }
        private void frmCheckOut_Load(object sender, EventArgs e)
        {
            tpStart.Time = DateTime.Now;
            tpEnd.Time = DateTime.Now;
            #region Load Combos
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT vhclNo FROM tblvehicle";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        cmbVehicleList.Properties.Items.Add(rdr[0].ToString());
                    }

                    comm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    comm = new MySqlCommand();
                    comm.Connection = con;
                    comm.CommandText = "SELECT Name FROM tbldrivers";
                    MySqlDataReader rdr = comm.ExecuteReader();
                    while (rdr.Read())
                    {
                        cmbDriverList.Properties.Items.Add(rdr[0].ToString());
                    }

                    comm.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion

            cmbDriverList.Enabled = false;
            txtDriverCharges.Enabled = false;
            txtExCharges.Enabled = false;
            txtpetrolcharg.Enabled = false;
            lblPetrolValue.Text = "0";
        }
        private void AddCst_Click(object sender, EventArgs e)
        {
            frmCustomer frm = new frmCustomer();
            frm.Show();
        }
        private void chkDriver_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkDriver.Checked != true)
            {
                txtDriverCharges.Enabled = false;
            }

            else
            {
                cmbDriverList.Enabled = true;
                txtDriverCharges.Enabled = true;
                cmbDriverList.Focus();
            }
        }
        private void checkEdit1_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked != true)
            {
                txtExCharges.Enabled = false;
            }
            else
            {
                txtExCharges.Enabled = true;
                txtExCharges.Focus();
            }
        }
        private void btnPay_Click(object sender, EventArgs e)
        {
            double tot = double.Parse(lblGrandTotal.Text);
            double due = double.Parse(lblDue.Text);

            SetPayEventArgs.TotAmount = tot;
            SetPayEventArgs.DuePayment = due;

            frmPay paynow = new frmPay();
            paynow.PayDone += paynow_PayDone;

            paynow.ShowDialog();
        }
        void paynow_PayDone(object sender, PayDoneEventArgs e)
        {
            ListViewItem itm = new ListViewItem(e.PayTime.ToString());
            itm.SubItems.Add(e.PayMethod.ToString());
            itm.SubItems.Add(e.CardName.ToString());
            itm.SubItems.Add(e.CardRef.ToString());
            itm.SubItems.Add(e.Payment.ToString());
            lstPayments.Items.Add(itm);

            lblPaid.Text = this.calPaid();
        }
        private void button1_Click(object sender, EventArgs e)
        {

        }
        private void PicCustomer_DoubleClick(object sender, EventArgs e)
        {
            Forms.XtraForm2 frm = new XtraForm2();
            frm.Show();
        }
        private void textEdit1_KeyDown_1(object sender, KeyEventArgs e)
        {

        }
        private void gaugeControl1_MouseDown(object sender, MouseEventArgs e)
        {
            BasePrimitiveHitInfo hi = (gaugeControl1 as IGaugeContainer).CalcHitInfo(e.Location);
            if (hi.Element != null && !hi.Element.IsComposite) 
            {
            //move gauge needle 
            }
       }
        private void checkEdit5_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkEdit5.Checked != true)
            {
                txtpetrolcharg.Enabled = false;
            }
            else
            {
                txtpetrolcharg.Enabled = true;
                txtpetrolcharg.Focus();
            }
        }
        private void cmbDriverList_SelectedValueChanged(object sender, EventArgs e)
        {
            txtDriverCharges.Focus();
        }
        private void dateNavigator1_SelectionChanged(object sender, EventArgs e)
        {
            dtpStart.Value = dateNavigator1.SelectionStart;
            dtpEnd.Value = dateNavigator1.SelectionEnd - TimeSpan.FromDays(1);
            txtShortTime.Text = "";
            txtShortTime.Focus();
        }
        private void dtpEnd_ValueChanged(object sender, EventArgs e)
        {
            tpEnd.Time = DateTime.Now;
            txtDays.Text = this.CountTime().Days.ToString();
            txtHrs.Text = this.CountTime().Hours.ToString();
        }
        private void tpEnd_EditValueChanged(object sender, EventArgs e)
        {
            txtHrs.Text = this.CountTime().Hours.ToString();
            txtDays.Text = this.CountTime().Days.ToString();
        }
        private void txtShortTime_KeyDown(object sender, KeyEventArgs e)
        {
            int hrs;
            if (int.TryParse(txtShortTime.Text.Trim(),out hrs))
            {
                if (hrs<=12)
                {
                    if (e.KeyCode == Keys.Left)
                    {
                        if (hrs==12)
                        {
                            tpEnd.Time = DateTime.MinValue;
                        }
                        else
                        {
                            tpEnd.Time = DateTime.MinValue.AddHours(hrs);
                        }
                        cmbVehicleList.Focus();
                    }
                    if (e.KeyCode == Keys.Right)
                    {
                        if (hrs==12)
                        {
                            tpEnd.Time = DateTime.MinValue.AddHours(hrs);
                        }
                        else
                        {
                            tpEnd.Time = DateTime.MinValue.AddHours(hrs + 12);
                        }
                        cmbVehicleList.Focus();
                    }
                }
                else
                {
                    txtShortTime.Text = "";
                }
                
            }
            else
            {
                txtShortTime.Text = "";
            }
            
        }
        private void txtShortTime_KeyUp(object sender, KeyEventArgs e)
        {
            
        }
        private void chkDriver_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDriver.Checked==true)
            {
                cmbDriverList.Enabled = true;
                txtDriverCharges.Enabled = true;
                cmbDriverList.Focus();
            }
            else
            {
                cmbDriverList.Enabled = false;
                txtDriverCharges.Enabled = false;
                cmbDriverList.Text = "";
                txtDriverCharges.Text = "";
            }
        }
        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked==true)
            {
                txtExCharges.Enabled = true;
                txtExCharges.Focus();
            }
            else
            {
                txtExCharges.Enabled = false;
                txtExCharges.Text = "";
            }
        }
        private void checkEdit5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit5.Checked==true)
            {
                txtpetrolcharg.Enabled = true;
                txtpetrolcharg.Focus();
            }
            else
            {
                txtpetrolcharg.Enabled = false;
                txtpetrolcharg.Text = "";
            }
        }
        private void cmbVehicleList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.loadVhclDetails(cmbVehicleList.Text.Trim());
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region Add to Appointmant
            int resID = this.GetResourceID(cmbVehicleList.Text.Trim());
            int apID = int.Parse(this.GetLastID("`tblappointment`").ToString());
            try
            {
                using (con = new MySqlConnection(AppSett.CS))
                {
                    string sql = "INSERT INTO `appointments` (`UniqeID`,`Type`,`StartDate`,`EndDate`,`AllDay`,`Subject`,`Location`,`Description`,`Status`,`Lable`,`ResourceID`) VALUES (@UniqeID,@Type,@StartDate,@EndDate,@AllDay,@Subject,@Location,@Description,@Status,@Lable,@ResourceID)";
                    comm = new MySqlCommand(sql, con);
                    comm.Parameters.AddWithValue("@UniqeID", apID);
                    comm.Parameters.AddWithValue("@Type", 0);
                    comm.Parameters.AddWithValue("@StartDate", dtpStart.Value.AddHours(tpStart.Time.Hour).AddMinutes(tpStart.Time.Minute));
                    comm.Parameters.AddWithValue("@EndDate", dtpEnd.Value.AddHours(tpEnd.Time.Hour).AddMinutes(tpEnd.Time.Minute));
                    comm.Parameters.AddWithValue("@AllDay", 0);
                    comm.Parameters.AddWithValue("@Subject", txtFName.Text.Trim());
                    comm.Parameters.AddWithValue("@Location", txtContact1.Text.Trim());
                    comm.Parameters.AddWithValue("@Description", txtVhclNo.Text.Trim());
                    comm.Parameters.AddWithValue("@Status", 0);
                    comm.Parameters.AddWithValue("@Lable", 5);
                    comm.Parameters.AddWithValue("@ResourceID", resID);
                    con.Open();
                    comm.ExecuteNonQuery();
                    this.UpdateID("`tblappointment`", this.GetLastID("`tblappointment`"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion
            #region Add to Ceckout
            try
            {
                string chkID = string.Format("OUT{0}", this.GetLastID("`tblCheckout`").ToString());
                using (con = new MySqlConnection(AppSett.CS))
                {
                    string sql = "INSERT INTO `tblchekout` (`chekoutID`,`VehicleNo`,`VehicleType`,`CustomerNIC`,`CustomerName`,`DriverID`,`DriverName`,`DrivaerRate`,`StartDateTime`,`EndDatetime`,`Days`,`Hours`,`LimitKm`,`RatePerKm`,`DecoRate`,`FuelLevel`,`FuelAmount`,`GrandTotal`,`PayedAmount`,`DueAmount`,`apID`) VALUES (@chekoutID,@VehicleNo,@VehicleType,@CustomerNIC,@CustomerName,@DriverID,@DriverName,@DrivaerRate,@StartDateTime,@EndDatetime,@Days,@Hours,@LimitKm,@RatePerKm,@DecoRate,@FuelLevel,@FuelAmount,@GrandTotal,@PayedAmount,@DueAmount,@apID)";
                    comm = new MySqlCommand(sql, con);
                    comm.Parameters.AddWithValue("@chekoutID",chkID);
                    comm.Parameters.AddWithValue("@VehicleNo",txtVhclNo.Text.Trim());
                    comm.Parameters.AddWithValue("@VehicleType","Car");
                    comm.Parameters.AddWithValue("@CustomerNIC",txtNIC.Text.Trim());
                    comm.Parameters.AddWithValue("@CustomerName",txtFName.Text.Trim());
                    comm.Parameters.AddWithValue("@DriverID",cmbDriverList.Text.Trim());
                    comm.Parameters.AddWithValue("@DriverName",cmbDriverList.Text.Trim());
                    comm.Parameters.AddWithValue("@DrivaerRate",txtDriverCharges.Text);
                    comm.Parameters.AddWithValue("@StartDateTime",dtpStart.Value.AddHours(tpStart.Time.Hour).AddMinutes(tpStart.Time.Minute));
                    comm.Parameters.AddWithValue("@EndDatetime",dtpEnd.Value.AddHours(tpEnd.Time.Hour).AddMinutes(tpEnd.Time.Minute));
                    comm.Parameters.AddWithValue("@Days",int.Parse(txtDays.Text.Trim()));
                    comm.Parameters.AddWithValue("@Hours",int.Parse(txtHrs.Text.Trim()));
                    comm.Parameters.AddWithValue("@LimitKm",int.Parse(txtDisLimit.Text.Trim()));
                    comm.Parameters.AddWithValue("@RatePerKm",double.Parse(txtExKmCharge.Text.Trim()));
                    comm.Parameters.AddWithValue("@DecoRate",double.Parse(lbldecor.Text));
                    comm.Parameters.AddWithValue("@FuelLevel",int.Parse(lblPetrolValue.Text));
                    comm.Parameters.AddWithValue("@FuelAmount",double.Parse(txtpetrolcharg.Text.Trim()));
                    comm.Parameters.AddWithValue("@GrandTotal",double.Parse(lblGrandTotal.Text));
                    comm.Parameters.AddWithValue("@PayedAmount",double.Parse(lblPaid.Text));
                    comm.Parameters.AddWithValue("@DueAmount",double.Parse(lblDue.Text));
                    comm.Parameters.AddWithValue("@apID", apID);
                    con.Open();
                    comm.ExecuteNonQuery();
                    this.UpdateID("`tblCheckout`", this.GetLastID("`tblCheckout`"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion
            MessageBox.Show("Data Saved");
        }
        
        private void txtDriverCharges_Leave(object sender, EventArgs e)
        {
            
        }
        private void txtExCharges_Leave(object sender, EventArgs e)
        {
            
        }
        private void txtpetrolcharg_Leave(object sender, EventArgs e)
        {
            
        }
        void CalculateGaugeValue(IGaugeContainer container, IConvertibleScaleEx scale, MouseEventArgs e)
        {
            BasePrimitiveHitInfo hi = container.CalcHitInfo(e.Location);
            if (hi.Element != null && !hi.Element.IsComposite)
            {
                PointF modelPt = MathHelper.PointToModelPoint(scale as IElement<IRenderableElement>, new PointF(e.X, e.Y));
                float percent = scale.PointToPercent(modelPt);
                scale.Value = scale.PercentToValue(percent);
                int FULL = Convert.ToInt32(scale.Value);
                lblPetrolValue.Text = FULL.ToString();
                txtpetrolcharg.Text = (FULL * 500).ToString();

            }
        }
        void ChangeCursor(IGaugeContainer container, MouseEventArgs e)
        {
            BasePrimitiveHitInfo hi = container.CalcHitInfo(e.Location);
            Cursor cursor = (hi.Element != null && !hi.Element.IsComposite) ? Cursors.Hand : Cursors.Default;
            if (((Control)container).Cursor != cursor)
                ((Control)container).Cursor = cursor;
        }
        private void gaugeControl1_MouseMove_1(object sender, MouseEventArgs e)
        {
            ChangeCursor(gaugeControl1 as IGaugeContainer, e);
            if (e.Button == MouseButtons.Left)
            {
                CalculateGaugeValue(gaugeControl1 as IGaugeContainer, arcScaleComponent1, e);

            }
        }
        private void lblRental_TextChanged(object sender, EventArgs e)
        {
            double rental = double.Parse(lblRental.Text);
            double drv = double.Parse(lblDriverCharge.Text);
            double decor = double.Parse(lbldecor.Text);
            lblGrandTotal.Text = AppSett.FormatCurrency((rental+drv+decor).ToString());
        }
        private void lblDriverCharge_TextChanged(object sender, EventArgs e)
        {
            double rental = double.Parse(lblRental.Text);
            double drv = double.Parse(lblDriverCharge.Text);
            double decor = double.Parse(lbldecor.Text);
            lblGrandTotal.Text = AppSett.FormatCurrency((rental + drv + decor).ToString());
        }
        private void lbldecor_TextChanged(object sender, EventArgs e)
        {
            double rental = double.Parse(lblRental.Text);
            double drv = double.Parse(lblDriverCharge.Text);
            double decor = double.Parse(lbldecor.Text);
            lblGrandTotal.Text = AppSett.FormatCurrency((rental + drv + decor).ToString());
        }

        private void cmbVehicleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVehicleList.SelectedIndex!=-1)
            {
                this.loadVhclDetails(cmbVehicleList.Text.Trim());
                txtCurODO.Focus();
            }
            
        }

        private void txtCurODO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                txtDisLimit.Focus();
            }
        }

        private void txtDisLimit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtDayRate.Focus();
            }
        }

        private void txtNIC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.loadCustDetails(txtNIC.Text.Trim());
            }
        }

        private void txtDayRate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                double rate;
                double rental = 0;
                if (double.TryParse(txtDayRate.Text.Trim(),out rate))
                {
                    rental = (double.Parse(txtDays.Text.Trim()) * rate) + (double.Parse(txtHrs.Text.Trim()) * rate / 24);
                    lblRental.Text = AppSett.FormatCurrency(rental.ToString());
                }
            }
        }

        private void cmbDriverList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDriverList.SelectedIndex != -1)
            {
                txtDriverCharges.Focus();
            }
        }

        private void txtDriverCharges_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                double rate;
                double DRrental = 0;
                if (double.TryParse(txtDriverCharges.Text.Trim(), out rate))
                {
                    DRrental = (double.Parse(txtDays.Text.Trim()) * rate) + (double.Parse(txtHrs.Text.Trim()) * rate / 24);
                    lblDriverCharge.Text = AppSett.FormatCurrency(DRrental.ToString());
                }
            }
        }

        private void txtExCharges_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                double DCharge;
                if (double.TryParse(txtExCharges.Text.Trim(), out DCharge))
                {
                    lbldecor.Text = AppSett.FormatCurrency(DCharge.ToString());
                }
            }
        }

        private void btnPay_Click_1(object sender, EventArgs e)
        {
            double tot = double.Parse(lblGrandTotal.Text);
            double due = double.Parse(lblDue.Text);

            SetPayEventArgs.TotAmount = tot;
            SetPayEventArgs.DuePayment = due;

            frmPay paynow = new frmPay();
            paynow.PayDone += paynow_PayDone;

            paynow.ShowDialog();
        }
    }
      
}
        
        
