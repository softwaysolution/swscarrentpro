﻿namespace CarRentPro.Forms
{
    partial class frmShowRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.vGridcustromer = new DevExpress.XtraVerticalGrid.VGridControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.vGridVehicleDtl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.vGridDriverDtl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.vGridtrancationdlt = new DevExpress.XtraVerticalGrid.VGridControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.vGridPaymentdlt = new DevExpress.XtraVerticalGrid.VGridControl();
            this.txtCheID = new System.Windows.Forms.TextBox();
            this.txtVehID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridcustromer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicleDtl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridDriverDtl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridtrancationdlt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridPaymentdlt)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.vGridcustromer);
            this.groupControl3.Location = new System.Drawing.Point(3, 2);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(210, 122);
            this.groupControl3.TabIndex = 6;
            this.groupControl3.Text = "Customer List";
            // 
            // vGridcustromer
            // 
            this.vGridcustromer.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridcustromer.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridcustromer.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridcustromer.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.Category.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.Category.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.Category.Options.UseFont = true;
            this.vGridcustromer.Appearance.Category.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridcustromer.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridcustromer.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.Empty.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridcustromer.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridcustromer.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridcustromer.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridcustromer.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridcustromer.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridcustromer.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridcustromer.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridcustromer.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridcustromer.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridcustromer.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridcustromer.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridcustromer.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridcustromer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridcustromer.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridcustromer.Location = new System.Drawing.Point(2, 20);
            this.vGridcustromer.Name = "vGridcustromer";
            this.vGridcustromer.Size = new System.Drawing.Size(206, 100);
            this.vGridcustromer.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.vGridVehicleDtl);
            this.groupControl1.Location = new System.Drawing.Point(217, 100);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(214, 220);
            this.groupControl1.TabIndex = 7;
            this.groupControl1.Text = "Vehicle Deatils";
            // 
            // vGridVehicleDtl
            // 
            this.vGridVehicleDtl.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridVehicleDtl.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridVehicleDtl.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridVehicleDtl.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.Category.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseFont = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridVehicleDtl.Appearance.Empty.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridVehicleDtl.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridVehicleDtl.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridVehicleDtl.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridVehicleDtl.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridVehicleDtl.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridVehicleDtl.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridVehicleDtl.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridVehicleDtl.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridVehicleDtl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridVehicleDtl.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridVehicleDtl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridVehicleDtl.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridVehicleDtl.Location = new System.Drawing.Point(2, 20);
            this.vGridVehicleDtl.Name = "vGridVehicleDtl";
            this.vGridVehicleDtl.Size = new System.Drawing.Size(210, 198);
            this.vGridVehicleDtl.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.vGridDriverDtl);
            this.groupControl2.Location = new System.Drawing.Point(217, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(212, 94);
            this.groupControl2.TabIndex = 8;
            this.groupControl2.Text = "Driver Deatils";
            // 
            // vGridDriverDtl
            // 
            this.vGridDriverDtl.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridDriverDtl.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridDriverDtl.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridDriverDtl.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.Category.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.Category.Options.UseBorderColor = true;
            this.vGridDriverDtl.Appearance.Category.Options.UseFont = true;
            this.vGridDriverDtl.Appearance.Category.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridDriverDtl.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridDriverDtl.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridDriverDtl.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridDriverDtl.Appearance.Empty.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridDriverDtl.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridDriverDtl.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridDriverDtl.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridDriverDtl.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridDriverDtl.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridDriverDtl.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridDriverDtl.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridDriverDtl.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridDriverDtl.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridDriverDtl.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridDriverDtl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridDriverDtl.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridDriverDtl.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridDriverDtl.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridDriverDtl.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridDriverDtl.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridDriverDtl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridDriverDtl.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridDriverDtl.Location = new System.Drawing.Point(2, 20);
            this.vGridDriverDtl.Name = "vGridDriverDtl";
            this.vGridDriverDtl.Size = new System.Drawing.Size(208, 72);
            this.vGridDriverDtl.TabIndex = 0;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.vGridtrancationdlt);
            this.groupControl4.Location = new System.Drawing.Point(3, 128);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(212, 190);
            this.groupControl4.TabIndex = 9;
            this.groupControl4.Text = "Trancation Deatils";
            // 
            // vGridtrancationdlt
            // 
            this.vGridtrancationdlt.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridtrancationdlt.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridtrancationdlt.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridtrancationdlt.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.Category.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.Category.Options.UseBorderColor = true;
            this.vGridtrancationdlt.Appearance.Category.Options.UseFont = true;
            this.vGridtrancationdlt.Appearance.Category.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridtrancationdlt.Appearance.Empty.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridtrancationdlt.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridtrancationdlt.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridtrancationdlt.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridtrancationdlt.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridtrancationdlt.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridtrancationdlt.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridtrancationdlt.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridtrancationdlt.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridtrancationdlt.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridtrancationdlt.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridtrancationdlt.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridtrancationdlt.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridtrancationdlt.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridtrancationdlt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridtrancationdlt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridtrancationdlt.Location = new System.Drawing.Point(2, 20);
            this.vGridtrancationdlt.Name = "vGridtrancationdlt";
            this.vGridtrancationdlt.Size = new System.Drawing.Size(208, 168);
            this.vGridtrancationdlt.TabIndex = 0;
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.vGridPaymentdlt);
            this.groupControl5.Location = new System.Drawing.Point(3, 326);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(428, 99);
            this.groupControl5.TabIndex = 9;
            this.groupControl5.Text = "Paymet deatils";
            // 
            // vGridPaymentdlt
            // 
            this.vGridPaymentdlt.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridPaymentdlt.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridPaymentdlt.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridPaymentdlt.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridPaymentdlt.Appearance.Category.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseFont = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridPaymentdlt.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.Empty.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridPaymentdlt.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridPaymentdlt.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridPaymentdlt.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridPaymentdlt.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridPaymentdlt.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridPaymentdlt.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridPaymentdlt.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridPaymentdlt.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridPaymentdlt.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridPaymentdlt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridPaymentdlt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridPaymentdlt.Location = new System.Drawing.Point(2, 20);
            this.vGridPaymentdlt.Name = "vGridPaymentdlt";
            this.vGridPaymentdlt.Size = new System.Drawing.Size(424, 77);
            this.vGridPaymentdlt.TabIndex = 0;
            // 
            // txtCheID
            // 
            this.txtCheID.Location = new System.Drawing.Point(314, 22);
            this.txtCheID.Name = "txtCheID";
            this.txtCheID.Size = new System.Drawing.Size(100, 21);
            this.txtCheID.TabIndex = 10;
            // 
            // txtVehID
            // 
            this.txtVehID.Location = new System.Drawing.Point(314, 50);
            this.txtVehID.Name = "txtVehID";
            this.txtVehID.Size = new System.Drawing.Size(100, 21);
            this.txtVehID.TabIndex = 11;
            // 
            // frmShowRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 433);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.txtVehID);
            this.Controls.Add(this.txtCheID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmShowRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Record ";
            this.Load += new System.EventHandler(this.frmShowRecord_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridcustromer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicleDtl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridDriverDtl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridtrancationdlt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridPaymentdlt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraVerticalGrid.VGridControl vGridcustromer;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraVerticalGrid.VGridControl vGridVehicleDtl;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraVerticalGrid.VGridControl vGridDriverDtl;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraVerticalGrid.VGridControl vGridtrancationdlt;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraVerticalGrid.VGridControl vGridPaymentdlt;
        public System.Windows.Forms.TextBox txtCheID;
        public System.Windows.Forms.TextBox txtVehID;
    }
}