﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;

namespace CarRentPro.Forms
{
    public partial class frmShowRecord : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataAdapter DA;
        DataSet DS = new DataSet();

       
        public frmShowRecord()
        {
            InitializeComponent();
        }
        private void LoadData(string chkID,string vehID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                con.Open();
                // customer Deatils
                DA = new MySqlDataAdapter(" SELECT `chekoutID`, `VehicleNo`, `VehicleType`, `CustomerNIC`, `CustomerName`FROM`carrentpro`.`tblchekout` WHERE `apID` ='" + chkID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridcustromer.DataSource = DS.Tables[0];
                //Driver Deatils
                DA = new MySqlDataAdapter(" SELECT`DriverID`, `DriverName`, `DrivaerRate`FROM`carrentpro`.`tblchekout` WHERE `apID` ='" + chkID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridDriverDtl.DataSource = DS.Tables[0];
                //TRANCATION DEATILS
                DA = new MySqlDataAdapter(" SELECT`StartDateTime`, `EndDatetime`, `Days`, `Hours`, `LimitKm`, `RatePerKm`, `DecoRate`, `FuelLevel`FROM`carrentpro`.`tblchekout` WHERE `apID` ='" + chkID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridtrancationdlt.DataSource = DS.Tables[0];

                //Payment Deatils
                DA = new MySqlDataAdapter(" SELECT GrandTotal, PayedAmount, DueAmount FROM carrentpro.tblchekout WHERE `apID` ='" + chkID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridPaymentdlt.DataSource = DS.Tables[0];

                //Vehicle Deatils


                /*cmd = new MySqlCommand("SELECT (vchlId) as [ID], (vhclNo) as [Veh: No], (vhclMake)  as [Mark], (vhclType) as [Type], (vchlModel) as [Model], (vchlOdoReading) as [ODO Reading], (Image1) as [Image] FROM tblvehicle WHERE vhclNo ='AA-1111'",con);
                DA = new MySqlDataAdapter(cmd);
                DS = new DataSet();
                DA.Fill(DS, "tblvehicle");
                vGridVehicleDtl.DataSource = DS.Tables["tblvehicle"];*/

                DA = new MySqlDataAdapter(" SELECT`vhclNo`, `vhclMake`, `vhclType`, `vchlModel`, `vchlOdoReading`, `Image1`FROM`carrentpro`.`tblvehicle` WHERE vhclNo ='" + vehID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridVehicleDtl.DataSource = DS.Tables[0];


            }
        }
        private void lstCustList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmShowRecord_Load(object sender, EventArgs e)
        {
            LoadData(txtCheID.Text, txtVehID.Text);

        }
    }
}