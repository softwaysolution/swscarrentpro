﻿namespace CarRentPro.Forms
{
    partial class frmCheckIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtagreementNo = new DevExpress.XtraEditors.TextEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.vGridPaymentdlt = new DevExpress.XtraVerticalGrid.VGridControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.vGridtrancationdlt = new DevExpress.XtraVerticalGrid.VGridControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.vGridDriverDtl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.vGridVehicleDtl = new DevExpress.XtraVerticalGrid.VGridControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.vGridcustromer = new DevExpress.XtraVerticalGrid.VGridControl();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.cGauge1 = new DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge();
            this.arcScaleBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleNeedleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleComponent2 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.txtexdays = new DevExpress.XtraEditors.TextEdit();
            this.txtExKM = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txttotalKM = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtexhours = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txttotaldays = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dtpDateNow = new System.Windows.Forms.DateTimePicker();
            this.tmtimeNow = new DevExpress.XtraEditors.TimeEdit();
            this.txtodoreadingEnd = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtagreementNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridPaymentdlt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridtrancationdlt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridDriverDtl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicleDtl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vGridcustromer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexdays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExKM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotalKM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexhours.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotaldays.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tmtimeNow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtodoreadingEnd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(6, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(115, 18);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Agreement No :";
            // 
            // txtagreementNo
            // 
            this.txtagreementNo.Location = new System.Drawing.Point(148, 4);
            this.txtagreementNo.Name = "txtagreementNo";
            this.txtagreementNo.Size = new System.Drawing.Size(280, 20);
            this.txtagreementNo.TabIndex = 1;
            this.txtagreementNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtagreementNo_KeyDown);
            this.txtagreementNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.txtagreementNo_MouseDown);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.vGridPaymentdlt);
            this.groupControl5.Location = new System.Drawing.Point(4, 354);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(428, 99);
            this.groupControl5.TabIndex = 21;
            this.groupControl5.Text = "Paymet deatils";
            // 
            // vGridPaymentdlt
            // 
            this.vGridPaymentdlt.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(200)))), ((int)(((byte)(169)))));
            this.vGridPaymentdlt.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(200)))), ((int)(((byte)(169)))));
            this.vGridPaymentdlt.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.vGridPaymentdlt.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.Category.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseFont = true;
            this.vGridPaymentdlt.Appearance.Category.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(200)))), ((int)(((byte)(169)))));
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(200)))), ((int)(((byte)(169)))));
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(220)))), ((int)(((byte)(189)))));
            this.vGridPaymentdlt.Appearance.Empty.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(214)))), ((int)(((byte)(189)))));
            this.vGridPaymentdlt.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(214)))), ((int)(((byte)(189)))));
            this.vGridPaymentdlt.Appearance.ExpandButton.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridPaymentdlt.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.vGridPaymentdlt.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(150)))));
            this.vGridPaymentdlt.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(151)))), ((int)(((byte)(100)))));
            this.vGridPaymentdlt.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.vGridPaymentdlt.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.vGridPaymentdlt.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.vGridPaymentdlt.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(200)))), ((int)(((byte)(169)))));
            this.vGridPaymentdlt.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RecordValue.BackColor = System.Drawing.Color.White;
            this.vGridPaymentdlt.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridPaymentdlt.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(151)))), ((int)(((byte)(100)))));
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(227)))), ((int)(((byte)(211)))));
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridPaymentdlt.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridPaymentdlt.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(200)))), ((int)(((byte)(169)))));
            this.vGridPaymentdlt.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridPaymentdlt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridPaymentdlt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridPaymentdlt.Location = new System.Drawing.Point(2, 20);
            this.vGridPaymentdlt.Name = "vGridPaymentdlt";
            this.vGridPaymentdlt.Size = new System.Drawing.Size(424, 77);
            this.vGridPaymentdlt.TabIndex = 0;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.vGridtrancationdlt);
            this.groupControl4.Location = new System.Drawing.Point(4, 156);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(212, 190);
            this.groupControl4.TabIndex = 22;
            this.groupControl4.Text = "Trancation Deatils";
            // 
            // vGridtrancationdlt
            // 
            this.vGridtrancationdlt.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridtrancationdlt.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridtrancationdlt.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridtrancationdlt.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.Category.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.Category.Options.UseBorderColor = true;
            this.vGridtrancationdlt.Appearance.Category.Options.UseFont = true;
            this.vGridtrancationdlt.Appearance.Category.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridtrancationdlt.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridtrancationdlt.Appearance.Empty.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridtrancationdlt.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridtrancationdlt.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridtrancationdlt.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridtrancationdlt.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridtrancationdlt.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridtrancationdlt.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridtrancationdlt.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridtrancationdlt.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridtrancationdlt.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridtrancationdlt.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridtrancationdlt.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridtrancationdlt.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridtrancationdlt.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridtrancationdlt.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridtrancationdlt.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridtrancationdlt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridtrancationdlt.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridtrancationdlt.Location = new System.Drawing.Point(2, 20);
            this.vGridtrancationdlt.Name = "vGridtrancationdlt";
            this.vGridtrancationdlt.Size = new System.Drawing.Size(208, 168);
            this.vGridtrancationdlt.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.vGridDriverDtl);
            this.groupControl2.Location = new System.Drawing.Point(218, 30);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(212, 94);
            this.groupControl2.TabIndex = 20;
            this.groupControl2.Text = "Driver Deatils";
            // 
            // vGridDriverDtl
            // 
            this.vGridDriverDtl.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridDriverDtl.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridDriverDtl.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridDriverDtl.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.Category.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.Category.Options.UseBorderColor = true;
            this.vGridDriverDtl.Appearance.Category.Options.UseFont = true;
            this.vGridDriverDtl.Appearance.Category.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridDriverDtl.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridDriverDtl.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridDriverDtl.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridDriverDtl.Appearance.Empty.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridDriverDtl.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridDriverDtl.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridDriverDtl.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridDriverDtl.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridDriverDtl.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridDriverDtl.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridDriverDtl.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridDriverDtl.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridDriverDtl.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridDriverDtl.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridDriverDtl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridDriverDtl.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridDriverDtl.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridDriverDtl.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridDriverDtl.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridDriverDtl.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridDriverDtl.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridDriverDtl.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridDriverDtl.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridDriverDtl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridDriverDtl.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridDriverDtl.Location = new System.Drawing.Point(2, 20);
            this.vGridDriverDtl.Name = "vGridDriverDtl";
            this.vGridDriverDtl.Size = new System.Drawing.Size(208, 72);
            this.vGridDriverDtl.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.vGridVehicleDtl);
            this.groupControl1.Location = new System.Drawing.Point(218, 128);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(214, 220);
            this.groupControl1.TabIndex = 19;
            this.groupControl1.Text = "Vehicle Deatils";
            // 
            // vGridVehicleDtl
            // 
            this.vGridVehicleDtl.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridVehicleDtl.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridVehicleDtl.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridVehicleDtl.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.Category.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseFont = true;
            this.vGridVehicleDtl.Appearance.Category.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridVehicleDtl.Appearance.Empty.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridVehicleDtl.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridVehicleDtl.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridVehicleDtl.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridVehicleDtl.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridVehicleDtl.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridVehicleDtl.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridVehicleDtl.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridVehicleDtl.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridVehicleDtl.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridVehicleDtl.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridVehicleDtl.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridVehicleDtl.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridVehicleDtl.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridVehicleDtl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridVehicleDtl.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridVehicleDtl.Location = new System.Drawing.Point(2, 20);
            this.vGridVehicleDtl.Name = "vGridVehicleDtl";
            this.vGridVehicleDtl.Size = new System.Drawing.Size(210, 198);
            this.vGridVehicleDtl.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.vGridcustromer);
            this.groupControl3.Location = new System.Drawing.Point(4, 30);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(210, 122);
            this.groupControl3.TabIndex = 18;
            this.groupControl3.Text = "Customer List";
            // 
            // vGridcustromer
            // 
            this.vGridcustromer.Appearance.Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridcustromer.Appearance.Category.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridcustromer.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.vGridcustromer.Appearance.Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.Category.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.Category.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.Category.Options.UseFont = true;
            this.vGridcustromer.Appearance.Category.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridcustromer.Appearance.CategoryExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridcustromer.Appearance.CategoryExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.CategoryExpandButton.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.Empty.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.ExpandButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridcustromer.Appearance.ExpandButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(125)))), ((int)(((byte)(186)))));
            this.vGridcustromer.Appearance.ExpandButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.ExpandButton.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.ExpandButton.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.ExpandButton.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.vGridcustromer.Appearance.FocusedCell.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedCell.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.FocusedRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(253)))));
            this.vGridcustromer.Appearance.FocusedRecord.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(87)))), ((int)(((byte)(138)))));
            this.vGridcustromer.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.vGridcustromer.Appearance.FocusedRow.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.FocusedRow.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(195)))));
            this.vGridcustromer.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridcustromer.Appearance.HorzLine.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RecordValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(252)))));
            this.vGridcustromer.Appearance.RecordValue.ForeColor = System.Drawing.Color.Black;
            this.vGridcustromer.Appearance.RecordValue.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RecordValue.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(145)))), ((int)(((byte)(186)))));
            this.vGridcustromer.Appearance.RowHeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(109)))), ((int)(((byte)(158)))));
            this.vGridcustromer.Appearance.RowHeaderPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(220)))));
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseBackColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseBorderColor = true;
            this.vGridcustromer.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vGridcustromer.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(167)))), ((int)(((byte)(178)))), ((int)(((byte)(185)))));
            this.vGridcustromer.Appearance.VertLine.Options.UseBackColor = true;
            this.vGridcustromer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vGridcustromer.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vGridcustromer.Location = new System.Drawing.Point(2, 20);
            this.vGridcustromer.Name = "vGridcustromer";
            this.vGridcustromer.Size = new System.Drawing.Size(206, 100);
            this.vGridcustromer.TabIndex = 0;
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.cGauge1});
            this.gaugeControl1.Location = new System.Drawing.Point(59, 146);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(147, 152);
            this.gaugeControl1.TabIndex = 59;
            this.gaugeControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gaugeControl1_MouseMove);
            // 
            // cGauge1
            // 
            this.cGauge1.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent[] {
            this.arcScaleBackgroundLayerComponent1});
            this.cGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 135, 140);
            this.cGauge1.Name = "cGauge1";
            this.cGauge1.Needles.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent[] {
            this.arcScaleNeedleComponent1});
            this.cGauge1.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent[] {
            this.arcScaleComponent1,
            this.arcScaleComponent2});
            // 
            // arcScaleBackgroundLayerComponent1
            // 
            this.arcScaleBackgroundLayerComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleBackgroundLayerComponent1.Name = "bg1";
            this.arcScaleBackgroundLayerComponent1.ScaleCenterPos = new DevExpress.XtraGauges.Core.Base.PointF2D(0.85F, 0.85F);
            this.arcScaleBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularQuarter_Style11Left;
            this.arcScaleBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // arcScaleComponent1
            // 
            this.arcScaleComponent1.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 14F);
            this.arcScaleComponent1.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(212F, 212F);
            this.arcScaleComponent1.EndAngle = -90F;
            this.arcScaleComponent1.MajorTickCount = 3;
            this.arcScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent1.MajorTickmark.ShapeOffset = -6F;
            this.arcScaleComponent1.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.6F, 1.6F);
            this.arcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_4;
            this.arcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent1.MaxValue = 10F;
            this.arcScaleComponent1.MinorTickCount = 4;
            this.arcScaleComponent1.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.6F, 1.6F);
            this.arcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_3;
            this.arcScaleComponent1.Name = "scale1";
            this.arcScaleComponent1.RadiusX = 176F;
            this.arcScaleComponent1.RadiusY = 176F;
            this.arcScaleComponent1.StartAngle = -180F;
            this.arcScaleComponent1.Value = 10F;
            // 
            // arcScaleNeedleComponent1
            // 
            this.arcScaleNeedleComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleNeedleComponent1.EndOffset = -12F;
            this.arcScaleNeedleComponent1.Name = "needle1";
            this.arcScaleNeedleComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_Style11;
            this.arcScaleNeedleComponent1.StartOffset = -17.5F;
            this.arcScaleNeedleComponent1.ZOrder = -50;
            // 
            // arcScaleComponent2
            // 
            this.arcScaleComponent2.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.arcScaleComponent2.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(212F, 212F);
            this.arcScaleComponent2.EndAngle = -90F;
            this.arcScaleComponent2.MajorTickCount = 4;
            this.arcScaleComponent2.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent2.MajorTickmark.ShapeOffset = -3F;
            this.arcScaleComponent2.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_2;
            this.arcScaleComponent2.MajorTickmark.TextOffset = -20F;
            this.arcScaleComponent2.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent2.MaxValue = 500F;
            this.arcScaleComponent2.MinorTickCount = 4;
            this.arcScaleComponent2.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_1;
            this.arcScaleComponent2.MinValue = 200F;
            this.arcScaleComponent2.Name = "scale2";
            this.arcScaleComponent2.RadiusX = 120F;
            this.arcScaleComponent2.RadiusY = 120F;
            this.arcScaleComponent2.StartAngle = -180F;
            this.arcScaleComponent2.Value = 200F;
            this.arcScaleComponent2.ZOrder = -1;
            // 
            // groupControl6
            // 
            this.groupControl6.Controls.Add(this.groupBox2);
            this.groupControl6.Controls.Add(this.groupBox1);
            this.groupControl6.Location = new System.Drawing.Point(434, 7);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(792, 446);
            this.groupControl6.TabIndex = 60;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelControl7);
            this.groupBox2.Controls.Add(this.groupControl7);
            this.groupBox2.Controls.Add(this.labelControl6);
            this.groupBox2.Controls.Add(this.labelControl5);
            this.groupBox2.Controls.Add(this.labelControl4);
            this.groupBox2.Controls.Add(this.labelControl2);
            this.groupBox2.Controls.Add(this.textEdit9);
            this.groupBox2.Controls.Add(this.txtexdays);
            this.groupBox2.Controls.Add(this.txtExKM);
            this.groupBox2.Controls.Add(this.labelControl15);
            this.groupBox2.Controls.Add(this.textEdit6);
            this.groupBox2.Controls.Add(this.labelControl14);
            this.groupBox2.Controls.Add(this.txttotalKM);
            this.groupBox2.Controls.Add(this.labelControl11);
            this.groupBox2.Controls.Add(this.txtexhours);
            this.groupBox2.Controls.Add(this.labelControl12);
            this.groupBox2.Controls.Add(this.txttotaldays);
            this.groupBox2.Controls.Add(this.labelControl13);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(241, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(549, 424);
            this.groupBox2.TabIndex = 63;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Different ";
            // 
            // groupControl7
            // 
            this.groupControl7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl7.Location = new System.Drawing.Point(3, 159);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(543, 262);
            this.groupControl7.TabIndex = 73;
            this.groupControl7.Text = "ExCharges";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(402, 136);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(27, 14);
            this.labelControl6.TabIndex = 72;
            this.labelControl6.Text = "00.00";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(402, 98);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(27, 14);
            this.labelControl5.TabIndex = 71;
            this.labelControl5.Text = "00.00";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(402, 70);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(27, 14);
            this.labelControl4.TabIndex = 70;
            this.labelControl4.Text = "00.00";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(402, 41);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 14);
            this.labelControl2.TabIndex = 69;
            this.labelControl2.Text = "00.00";
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(166, 118);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit9.Size = new System.Drawing.Size(60, 20);
            this.textEdit9.TabIndex = 68;
            // 
            // txtexdays
            // 
            this.txtexdays.Location = new System.Drawing.Point(166, 38);
            this.txtexdays.Name = "txtexdays";
            this.txtexdays.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexdays.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtexdays.Properties.Appearance.Options.UseFont = true;
            this.txtexdays.Properties.Appearance.Options.UseForeColor = true;
            this.txtexdays.Size = new System.Drawing.Size(60, 20);
            this.txtexdays.TabIndex = 67;
            // 
            // txtExKM
            // 
            this.txtExKM.Location = new System.Drawing.Point(166, 90);
            this.txtExKM.Name = "txtExKM";
            this.txtExKM.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExKM.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtExKM.Properties.Appearance.Options.UseFont = true;
            this.txtExKM.Properties.Appearance.Options.UseForeColor = true;
            this.txtExKM.Size = new System.Drawing.Size(60, 20);
            this.txtExKM.TabIndex = 65;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(177, 19);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(22, 13);
            this.labelControl15.TabIndex = 66;
            this.labelControl15.Text = "Ex  :";
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(92, 118);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit6.Size = new System.Drawing.Size(68, 20);
            this.textEdit6.TabIndex = 63;
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.Options.UseFont = true;
            this.labelControl14.Location = new System.Drawing.Point(6, 121);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(72, 14);
            this.labelControl14.TabIndex = 64;
            this.labelControl14.Text = "Fuel wastage :";
            // 
            // txttotalKM
            // 
            this.txttotalKM.Location = new System.Drawing.Point(92, 90);
            this.txttotalKM.Name = "txttotalKM";
            this.txttotalKM.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalKM.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txttotalKM.Properties.Appearance.Options.UseFont = true;
            this.txttotalKM.Properties.Appearance.Options.UseForeColor = true;
            this.txttotalKM.Size = new System.Drawing.Size(68, 20);
            this.txttotalKM.TabIndex = 61;
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(6, 98);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(89, 14);
            this.labelControl11.TabIndex = 62;
            this.labelControl11.Text = "Totak Km usage  :";
            // 
            // txtexhours
            // 
            this.txtexhours.Location = new System.Drawing.Point(166, 64);
            this.txtexhours.Name = "txtexhours";
            this.txtexhours.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexhours.Properties.Appearance.ForeColor = System.Drawing.Color.Red;
            this.txtexhours.Properties.Appearance.Options.UseFont = true;
            this.txtexhours.Properties.Appearance.Options.UseForeColor = true;
            this.txtexhours.Size = new System.Drawing.Size(60, 20);
            this.txtexhours.TabIndex = 5;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.Options.UseFont = true;
            this.labelControl12.Location = new System.Drawing.Point(6, 67);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(35, 14);
            this.labelControl12.TabIndex = 60;
            this.labelControl12.Text = "Hours :";
            // 
            // txttotaldays
            // 
            this.txttotaldays.Location = new System.Drawing.Point(92, 38);
            this.txttotaldays.Name = "txttotaldays";
            this.txttotaldays.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotaldays.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.txttotaldays.Properties.Appearance.Options.UseFont = true;
            this.txttotaldays.Properties.Appearance.Options.UseForeColor = true;
            this.txttotaldays.Size = new System.Drawing.Size(68, 20);
            this.txttotaldays.TabIndex = 5;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.Options.UseFont = true;
            this.labelControl13.Location = new System.Drawing.Point(6, 41);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(30, 14);
            this.labelControl13.TabIndex = 60;
            this.labelControl13.Text = "Date :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.dtpDateNow);
            this.groupBox1.Controls.Add(this.tmtimeNow);
            this.groupBox1.Controls.Add(this.txtodoreadingEnd);
            this.groupBox1.Controls.Add(this.gaugeControl1);
            this.groupBox1.Controls.Add(this.labelControl10);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.labelControl9);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(2, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(239, 424);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Current Record";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(39, 347);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 65;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtpDateNow
            // 
            this.dtpDateNow.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateNow.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateNow.Location = new System.Drawing.Point(92, 41);
            this.dtpDateNow.Name = "dtpDateNow";
            this.dtpDateNow.Size = new System.Drawing.Size(114, 22);
            this.dtpDateNow.TabIndex = 64;
            // 
            // tmtimeNow
            // 
            this.tmtimeNow.EditValue = new System.DateTime(2018, 1, 9, 0, 0, 0, 0);
            this.tmtimeNow.Location = new System.Drawing.Point(92, 67);
            this.tmtimeNow.Name = "tmtimeNow";
            this.tmtimeNow.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tmtimeNow.Properties.Appearance.Options.UseFont = true;
            this.tmtimeNow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tmtimeNow.Size = new System.Drawing.Size(114, 20);
            this.tmtimeNow.TabIndex = 63;
            // 
            // txtodoreadingEnd
            // 
            this.txtodoreadingEnd.Location = new System.Drawing.Point(92, 95);
            this.txtodoreadingEnd.Name = "txtodoreadingEnd";
            this.txtodoreadingEnd.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtodoreadingEnd.Properties.Appearance.Options.UseFont = true;
            this.txtodoreadingEnd.Size = new System.Drawing.Size(114, 20);
            this.txtodoreadingEnd.TabIndex = 61;
            this.txtodoreadingEnd.EditValueChanged += new System.EventHandler(this.txtodoreading_EditValueChanged);
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(6, 98);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(76, 14);
            this.labelControl10.TabIndex = 62;
            this.labelControl10.Text = "ODO Reading  :";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(6, 67);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(31, 14);
            this.labelControl3.TabIndex = 60;
            this.labelControl3.Text = "Time :";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(7, 41);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(30, 14);
            this.labelControl9.TabIndex = 60;
            this.labelControl9.Text = "Date :";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(104, 19);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(53, 13);
            this.labelControl7.TabIndex = 74;
            this.labelControl7.Text = "Tota Use  :";
            // 
            // frmCheckIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1229, 455);
            this.Controls.Add(this.groupControl6);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.txtagreementNo);
            this.Controls.Add(this.labelControl1);
            this.Name = "frmCheckIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Check In";
            this.Load += new System.EventHandler(this.frmCheckIn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtagreementNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridPaymentdlt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridtrancationdlt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridDriverDtl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridVehicleDtl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vGridcustromer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexdays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExKM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotalKM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtexhours.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttotaldays.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tmtimeNow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtodoreadingEnd.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtagreementNo;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraVerticalGrid.VGridControl vGridPaymentdlt;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraVerticalGrid.VGridControl vGridtrancationdlt;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraVerticalGrid.VGridControl vGridDriverDtl;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraVerticalGrid.VGridControl vGridVehicleDtl;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraVerticalGrid.VGridControl vGridcustromer;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge cGauge1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent2;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.TextEdit txtExKM;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txttotalKM;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtexhours;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txttotaldays;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.TextEdit txtodoreadingEnd;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit txtexdays;
        private System.Windows.Forms.DateTimePicker dtpDateNow;
        private DevExpress.XtraEditors.TimeEdit tmtimeNow;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
    }
}