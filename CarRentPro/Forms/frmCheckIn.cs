﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using DevExpress.XtraGauges.Base;
using DevExpress.XtraGauges.Core.Primitive;
using DevExpress.XtraGauges.Core.Model;
using DevExpress.XtraGauges.Core.Base;

namespace CarRentPro.Forms
{
    public partial class frmCheckIn : DevExpress.XtraEditors.XtraForm
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataAdapter DA;
        DataSet DS;
        int ODOReadingStart;
        int LimitKm;


        MySqlDataReader rdr;
        public frmCheckIn()
        {
            InitializeComponent();
        }
        private void LoadData(string chkID, string vehID)
        {
            using (con = new MySqlConnection(AppSett.CS))
            {
                con.Open();
                // customer Deatils
                DA = new MySqlDataAdapter(" SELECT `chekoutID`, `VehicleNo`, `VehicleType`, `CustomerNIC`, `CustomerName`FROM`carrentpro`.`tblchekout` WHERE `apID` ='" + chkID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridcustromer.DataSource = DS.Tables[0];
                //Driver Deatils
                DA = new MySqlDataAdapter(" SELECT`DriverID`, `DriverName`, `DrivaerRate`FROM`carrentpro`.`tblchekout` WHERE `apID` ='" + chkID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridDriverDtl.DataSource = DS.Tables[0];
                //TRANCATION DEATILS
                DA = new MySqlDataAdapter(" SELECT`StartDateTime`, `EndDatetime`, `Days`, `Hours`, `LimitKm`, `RatePerKm`, `DecoRate`, `FuelLevel`FROM`carrentpro`.`tblchekout` WHERE `apID` ='" + chkID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridtrancationdlt.DataSource = DS.Tables[0];

                //Payment Deatils
                DA = new MySqlDataAdapter(" SELECT GrandTotal, PayedAmount, DueAmount FROM carrentpro.tblchekout WHERE `apID` ='" + chkID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridPaymentdlt.DataSource = DS.Tables[0];

                //Vehicle Deatils


                DA = new MySqlDataAdapter(" SELECT`vhclNo`, `vhclMake`, `vhclType`, `vchlModel`, `vchlOdoReading`, `Image1`FROM`carrentpro`.`tblvehicle` WHERE vhclNo ='" + vehID + "'", con);
                DS = new DataSet();
                DA.Fill(DS);
                vGridVehicleDtl.DataSource = DS.Tables[0];


            }
        }
        private void frmCheckIn_Load(object sender, EventArgs e)
        {
            
        }

        private void txtagreementNo_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void txtagreementNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadData(txtagreementNo.Text, "AA-1111");
                dtpDateNow.Value = DateTime.Now;
                tmtimeNow.Time = DateTime.Now;
                txtodoreadingEnd.Focus();

                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    string sql = " SELECT`DrivaerRate`, `StartDateTime`, `EndDatetime`, `Days`, `Hours`, `LimitKm`, `RatePerKm`, `DecoRate`, `FuelLevel`, `FuelAmount`FROM`carrentpro`.`tblchekout` where `chekoutID` ";
                    cmd = new MySqlCommand(sql, con);
                   // rdr = cmd.EndExecuteReader();
                }
                using (con = new MySqlConnection(AppSett.CS))
                {
                    con.Open();
                    string sql = "SELECT * FROM carrentpro.tblchekout WHERE `apID` ='" + txtagreementNo.Text + "' ";
                    cmd = new MySqlCommand(sql, con);
                    rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            double DrivaerRate = double.Parse(rdr["DrivaerRate"].ToString());
                            DateTime StartDateTime = DateTime.Parse(rdr["StartDateTime"].ToString());
                            DateTime EndDatetime = DateTime.Parse(rdr["EndDatetime"].ToString());
                            int Days = int.Parse(rdr["Days"].ToString());
                            int Hours = int.Parse(rdr["Hours"].ToString());
                             LimitKm = int.Parse(rdr["LimitKm"].ToString());
                            double RatePerKm = double.Parse(rdr["RatePerKm"].ToString());
                            double DecoRate = double.Parse(rdr["DecoRate"].ToString());
                            int FuelLevel = int.Parse(rdr["FuelLevel"].ToString());
                            double FuelAmount = double.Parse(rdr["FuelAmount"].ToString());
                            double GrandTotal = double.Parse(rdr["GrandTotal"].ToString());
                            double PayedAmount = double.Parse(rdr["PayedAmount"].ToString());
                            double DueAmount = double.Parse(rdr["DueAmount"].ToString());
                            // odo readin Out  ? 

                             ODOReadingStart = 100000;

                            DateTime Sd = StartDateTime;
                            DateTime Ed = DateTime.Now;

                            TimeSpan t = Ed - Sd;
                            // int  NrOfDays =Convert.ToInt32( t);
                            txttotaldays.Text = t.Days.ToString();
                            txtexhours.Text = t.Hours.ToString();










                        }
                    }

                }


            }
                
        }

        void CalculateGaugeValue(IGaugeContainer container, IConvertibleScaleEx scale, MouseEventArgs e)
        {
            BasePrimitiveHitInfo hi = container.CalcHitInfo(e.Location);
            if (hi.Element != null && !hi.Element.IsComposite)
            {
                PointF modelPt = MathHelper.PointToModelPoint(scale as IElement<IRenderableElement>, new PointF(e.X, e.Y));
                float percent = scale.PointToPercent(modelPt);
                scale.Value = scale.PercentToValue(percent);
                int FULL = Convert.ToInt32(scale.Value);
                //lblPetrolValue.Text = FULL.ToString();
               // txtpetrolcharg.Text = (FULL * 500).ToString();

            }
        }
        void ChangeCursor(IGaugeContainer container, MouseEventArgs e)
        {
            BasePrimitiveHitInfo hi = container.CalcHitInfo(e.Location);
            Cursor cursor = (hi.Element != null && !hi.Element.IsComposite) ? Cursors.Hand : Cursors.Default;
            if (((Control)container).Cursor != cursor)
                ((Control)container).Cursor = cursor;
        }



        private void gaugeControl1_MouseMove(object sender, MouseEventArgs e)
        {
            ChangeCursor(gaugeControl1 as IGaugeContainer, e);
            if (e.Button == MouseButtons.Left)
            {
                CalculateGaugeValue(gaugeControl1 as IGaugeContainer, arcScaleComponent1, e);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
        }

        private void txtodoreading_EditValueChanged(object sender, EventArgs e)
        {
            int totalKm = int.Parse(txtodoreadingEnd.Text) - ODOReadingStart;
            int ExKm = totalKm - LimitKm;
            txttotalKM.Text = totalKm.ToString();
            txtExKM.Text = ExKm.ToString();
 
        }
    }
} 