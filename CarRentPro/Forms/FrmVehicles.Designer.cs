﻿namespace CarRentPro.Forms
{
    partial class FrmVehicles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVehicles));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.imageComboBoxEdit1 = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.imageSlider1 = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.txtPDay = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.txtDayLimit = new DevExpress.XtraEditors.TextEdit();
            this.txtHirePerKm = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtExtraChergePerKm = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.btnRateSave = new DevExpress.XtraEditors.SimpleButton();
            this.dtpLicExpDate = new System.Windows.Forms.DateTimePicker();
            this.dtpInsExpDate = new System.Windows.Forms.DateTimePicker();
            this.txtVhclLicNo = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclInsNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclNxtSrvice = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclCurOdo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtAdress2 = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.txtAdress1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact = new DevExpress.XtraEditors.TextEdit();
            this.txtNIC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.lstVhclFull = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblVhclID = new DevExpress.XtraEditors.LabelControl();
            this.btnVhclClear = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelVhcl = new DevExpress.XtraEditors.SimpleButton();
            this.btnUpdateVhcl = new DevExpress.XtraEditors.SimpleButton();
            this.dtpDelDate = new System.Windows.Forms.DateTimePicker();
            this.btnVhclSave = new DevExpress.XtraEditors.SimpleButton();
            this.picVhcl = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclMYeay = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclModel = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclMake = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnSupDelete = new DevExpress.XtraEditors.SimpleButton();
            this.lstSppliers = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblSupId = new DevExpress.XtraEditors.LabelControl();
            this.btnSupUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.btnSupSave = new DevExpress.XtraEditors.SimpleButton();
            this.lstVhcl = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.carrentproDataSet1 = new CarRentPro.carrentproDataSet();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.carrentproDataSet2 = new CarRentPro.carrentproDataSet();
            this.appointmentsTableAdapter1 = new CarRentPro.carrentproDataSetTableAdapters.appointmentsTableAdapter();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHirePerKm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraChergePerKm.Properties)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclLicNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclInsNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNxtSrvice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclCurOdo.Properties)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVhcl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.carrentproDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.carrentproDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.panelControl3);
            this.panelControl1.Controls.Add(this.panelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1284, 603);
            this.panelControl1.TabIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.xtraTabControl1);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(2, 341);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1280, 260);
            this.panelControl3.TabIndex = 1;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.xtraTabControl1.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1276, 256);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.listView2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1270, 228);
            this.xtraTabPage1.Text = "Maintenance";
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader24,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22});
            this.listView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView2.FullRowSelect = true;
            this.listView2.Location = new System.Drawing.Point(0, 0);
            this.listView2.MultiSelect = false;
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(1270, 228);
            this.listView2.TabIndex = 19;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Width = 0;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Vehicle No";
            this.columnHeader11.Width = 100;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Serviced date";
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader12.Width = 100;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Serv Mileage";
            this.columnHeader13.Width = 100;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Next Serv";
            this.columnHeader14.Width = 100;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Atf /CVT Mileage";
            this.columnHeader15.Width = 100;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Next Atf/Cvt Milg";
            this.columnHeader16.Width = 100;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Oil Filter";
            this.columnHeader17.Width = 100;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Other Filters";
            this.columnHeader18.Width = 150;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Engine Oil Type ";
            this.columnHeader19.Width = 150;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Gear Oil";
            this.columnHeader20.Width = 100;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Date";
            this.columnHeader21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader21.Width = 100;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Tayar ";
            this.columnHeader22.Width = 100;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.imageComboBoxEdit1);
            this.xtraTabPage2.Controls.Add(this.listView1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1270, 228);
            this.xtraTabPage2.Text = "Accident\'s Damages ";
            // 
            // imageComboBoxEdit1
            // 
            this.imageComboBoxEdit1.Location = new System.Drawing.Point(1031, 166);
            this.imageComboBoxEdit1.Name = "imageComboBoxEdit1";
            this.imageComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.imageComboBoxEdit1.Size = new System.Drawing.Size(100, 20);
            this.imageComboBoxEdit1.TabIndex = 21;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader23});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(167, 228);
            this.listView1.TabIndex = 19;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Date";
            this.columnHeader23.Width = 150;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1270, 228);
            this.xtraTabPage3.Text = " Insurance & Payment";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1270, 228);
            this.xtraTabPage4.Text = "xtraTabPage4";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.groupControl2);
            this.panelControl2.Controls.Add(this.groupControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(2, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1280, 339);
            this.panelControl2.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.imageSlider1);
            this.groupControl2.Controls.Add(this.labelControl26);
            this.groupControl2.Controls.Add(this.comboBoxEdit1);
            this.groupControl2.Controls.Add(this.labelControl25);
            this.groupControl2.Controls.Add(this.textEdit7);
            this.groupControl2.Controls.Add(this.pictureEdit4);
            this.groupControl2.Controls.Add(this.pictureEdit3);
            this.groupControl2.Controls.Add(this.pictureEdit2);
            this.groupControl2.Controls.Add(this.pictureEdit1);
            this.groupControl2.Controls.Add(this.xtraTabControl2);
            this.groupControl2.Controls.Add(this.simpleButton5);
            this.groupControl2.Controls.Add(this.simpleButton4);
            this.groupControl2.Controls.Add(this.simpleButton3);
            this.groupControl2.Controls.Add(this.simpleButton2);
            this.groupControl2.Controls.Add(this.labelControl22);
            this.groupControl2.Controls.Add(this.textEdit4);
            this.groupControl2.Controls.Add(this.labelControl21);
            this.groupControl2.Controls.Add(this.textEdit3);
            this.groupControl2.Controls.Add(this.labelControl20);
            this.groupControl2.Controls.Add(this.textEdit2);
            this.groupControl2.Controls.Add(this.labelControl19);
            this.groupControl2.Controls.Add(this.textEdit1);
            this.groupControl2.Controls.Add(this.lstVhclFull);
            this.groupControl2.Controls.Add(this.lblVhclID);
            this.groupControl2.Controls.Add(this.btnVhclClear);
            this.groupControl2.Controls.Add(this.btnDelVhcl);
            this.groupControl2.Controls.Add(this.btnUpdateVhcl);
            this.groupControl2.Controls.Add(this.dtpDelDate);
            this.groupControl2.Controls.Add(this.btnVhclSave);
            this.groupControl2.Controls.Add(this.picVhcl);
            this.groupControl2.Controls.Add(this.labelControl5);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl8);
            this.groupControl2.Controls.Add(this.txtVhclNo);
            this.groupControl2.Controls.Add(this.labelControl10);
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Controls.Add(this.labelControl9);
            this.groupControl2.Controls.Add(this.txtVhclMYeay);
            this.groupControl2.Controls.Add(this.txtVhclModel);
            this.groupControl2.Controls.Add(this.txtVhclMake);
            this.groupControl2.Controls.Add(this.txtVhclType);
            this.groupControl2.Controls.Add(this.simpleButton8);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(2, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(1276, 335);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Vehicle Deatils";
            // 
            // imageSlider1
            // 
            this.imageSlider1.Cursor = System.Windows.Forms.Cursors.Default;
            this.imageSlider1.Location = new System.Drawing.Point(536, 29);
            this.imageSlider1.Name = "imageSlider1";
            this.imageSlider1.Size = new System.Drawing.Size(234, 168);
            this.imageSlider1.TabIndex = 20;
            this.imageSlider1.Text = "imageSlider1";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(292, 208);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(40, 13);
            this.labelControl26.TabIndex = 55;
            this.labelControl26.Text = "Branch :";
            this.labelControl26.Visible = false;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(337, 205);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.Items.AddRange(new object[] {
            "Car",
            "Van",
            "Bike"});
            this.comboBoxEdit1.Size = new System.Drawing.Size(193, 20);
            this.comboBoxEdit1.TabIndex = 56;
            this.comboBoxEdit1.Visible = false;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(294, 162);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(38, 13);
            this.labelControl25.TabIndex = 53;
            this.labelControl25.Text = "Colour :";
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(338, 159);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit7.Size = new System.Drawing.Size(193, 20);
            this.textEdit7.TabIndex = 54;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit4.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit4.Location = new System.Drawing.Point(610, 75);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit4.Properties.InitialImage")));
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit4.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit4.Size = new System.Drawing.Size(127, 104);
            this.pictureEdit4.TabIndex = 52;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit3.Location = new System.Drawing.Point(575, 62);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit3.Properties.InitialImage")));
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit3.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit3.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit3.TabIndex = 51;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit2.Location = new System.Drawing.Point(564, 47);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit2.Properties.InitialImage")));
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit2.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit2.TabIndex = 50;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit1.Location = new System.Drawing.Point(545, 43);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit1.Properties.InitialImage")));
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit1.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit1.TabIndex = 49;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.xtraTabControl2.Location = new System.Drawing.Point(799, 20);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage5;
            this.xtraTabControl2.Size = new System.Drawing.Size(475, 313);
            this.xtraTabControl2.TabIndex = 48;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage5,
            this.xtraTabPage6,
            this.xtraTabPage7});
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.groupControl3);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(469, 285);
            this.xtraTabPage5.Text = "Rate Deatils";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.labelControl31);
            this.groupControl3.Controls.Add(this.labelControl30);
            this.groupControl3.Controls.Add(this.textEdit6);
            this.groupControl3.Controls.Add(this.labelControl28);
            this.groupControl3.Controls.Add(this.textEdit8);
            this.groupControl3.Controls.Add(this.textEdit5);
            this.groupControl3.Controls.Add(this.labelControl29);
            this.groupControl3.Controls.Add(this.txtPDay);
            this.groupControl3.Controls.Add(this.labelControl12);
            this.groupControl3.Controls.Add(this.simpleButton6);
            this.groupControl3.Controls.Add(this.txtDayLimit);
            this.groupControl3.Controls.Add(this.txtHirePerKm);
            this.groupControl3.Controls.Add(this.labelControl23);
            this.groupControl3.Controls.Add(this.labelControl14);
            this.groupControl3.Controls.Add(this.labelControl13);
            this.groupControl3.Controls.Add(this.labelControl24);
            this.groupControl3.Controls.Add(this.txtExtraChergePerKm);
            this.groupControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl3.Location = new System.Drawing.Point(0, 0);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(469, 285);
            this.groupControl3.TabIndex = 25;
            this.groupControl3.Text = "Rate Details";
            this.groupControl3.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl3_Paint);
            // 
            // txtPDay
            // 
            this.txtPDay.Location = new System.Drawing.Point(112, 78);
            this.txtPDay.Name = "txtPDay";
            this.txtPDay.Size = new System.Drawing.Size(82, 20);
            this.txtPDay.TabIndex = 42;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(135, 59);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(41, 13);
            this.labelControl12.TabIndex = 25;
            this.labelControl12.Text = "Per Day ";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(389, 231);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(75, 23);
            this.simpleButton6.TabIndex = 37;
            this.simpleButton6.Text = "Save";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // txtDayLimit
            // 
            this.txtDayLimit.Location = new System.Drawing.Point(112, 104);
            this.txtDayLimit.Name = "txtDayLimit";
            this.txtDayLimit.Size = new System.Drawing.Size(82, 20);
            this.txtDayLimit.TabIndex = 29;
            // 
            // txtHirePerKm
            // 
            this.txtHirePerKm.Location = new System.Drawing.Point(112, 162);
            this.txtHirePerKm.Name = "txtHirePerKm";
            this.txtHirePerKm.Size = new System.Drawing.Size(82, 20);
            this.txtHirePerKm.TabIndex = 33;
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(21, 137);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(85, 13);
            this.labelControl23.TabIndex = 30;
            this.labelControl23.Text = "Ex charge (1km) :";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(54, 82);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(53, 13);
            this.labelControl14.TabIndex = 27;
            this.labelControl14.Text = "Rate (Rs) :";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(16, 106);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(93, 13);
            this.labelControl13.TabIndex = 26;
            this.labelControl13.Text = "Distance limit (km) :";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(7, 165);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(99, 13);
            this.labelControl24.TabIndex = 32;
            this.labelControl24.Text = "Hire charge (Rs/km):";
            // 
            // txtExtraChergePerKm
            // 
            this.txtExtraChergePerKm.Location = new System.Drawing.Point(112, 134);
            this.txtExtraChergePerKm.Name = "txtExtraChergePerKm";
            this.txtExtraChergePerKm.Size = new System.Drawing.Size(82, 20);
            this.txtExtraChergePerKm.TabIndex = 31;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.groupControl4);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(469, 285);
            this.xtraTabPage6.Text = "Additional Deatils";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.btnRateSave);
            this.groupControl4.Controls.Add(this.dtpLicExpDate);
            this.groupControl4.Controls.Add(this.dtpInsExpDate);
            this.groupControl4.Controls.Add(this.txtVhclLicNo);
            this.groupControl4.Controls.Add(this.txtVhclInsNo);
            this.groupControl4.Controls.Add(this.labelControl18);
            this.groupControl4.Controls.Add(this.txtVhclNxtSrvice);
            this.groupControl4.Controls.Add(this.labelControl15);
            this.groupControl4.Controls.Add(this.txtVhclCurOdo);
            this.groupControl4.Controls.Add(this.labelControl16);
            this.groupControl4.Controls.Add(this.labelControl17);
            this.groupControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl4.Location = new System.Drawing.Point(0, 0);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(469, 285);
            this.groupControl4.TabIndex = 30;
            this.groupControl4.Text = "Additional Details";
            // 
            // btnRateSave
            // 
            this.btnRateSave.Location = new System.Drawing.Point(379, 245);
            this.btnRateSave.Name = "btnRateSave";
            this.btnRateSave.Size = new System.Drawing.Size(75, 23);
            this.btnRateSave.TabIndex = 36;
            this.btnRateSave.Text = "Save";
            this.btnRateSave.Click += new System.EventHandler(this.btnRateSave_Click);
            // 
            // dtpLicExpDate
            // 
            this.dtpLicExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLicExpDate.Location = new System.Drawing.Point(148, 169);
            this.dtpLicExpDate.Name = "dtpLicExpDate";
            this.dtpLicExpDate.Size = new System.Drawing.Size(95, 21);
            this.dtpLicExpDate.TabIndex = 35;
            // 
            // dtpInsExpDate
            // 
            this.dtpInsExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInsExpDate.Location = new System.Drawing.Point(148, 113);
            this.dtpInsExpDate.Name = "dtpInsExpDate";
            this.dtpInsExpDate.Size = new System.Drawing.Size(95, 21);
            this.dtpInsExpDate.TabIndex = 33;
            // 
            // txtVhclLicNo
            // 
            this.txtVhclLicNo.Location = new System.Drawing.Point(148, 143);
            this.txtVhclLicNo.Name = "txtVhclLicNo";
            this.txtVhclLicNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclLicNo.TabIndex = 34;
            // 
            // txtVhclInsNo
            // 
            this.txtVhclInsNo.Location = new System.Drawing.Point(148, 87);
            this.txtVhclInsNo.Name = "txtVhclInsNo";
            this.txtVhclInsNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclInsNo.TabIndex = 33;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(84, 149);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(58, 13);
            this.labelControl18.TabIndex = 30;
            this.labelControl18.Text = "License No :";
            // 
            // txtVhclNxtSrvice
            // 
            this.txtVhclNxtSrvice.Location = new System.Drawing.Point(148, 57);
            this.txtVhclNxtSrvice.Name = "txtVhclNxtSrvice";
            this.txtVhclNxtSrvice.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNxtSrvice.TabIndex = 29;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(30, 24);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(112, 13);
            this.labelControl15.TabIndex = 25;
            this.labelControl15.Text = "Current ODO Reading :";
            // 
            // txtVhclCurOdo
            // 
            this.txtVhclCurOdo.Location = new System.Drawing.Point(148, 23);
            this.txtVhclCurOdo.Name = "txtVhclCurOdo";
            this.txtVhclCurOdo.Size = new System.Drawing.Size(139, 20);
            this.txtVhclCurOdo.TabIndex = 25;
            this.txtVhclCurOdo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVhclCurOdo_KeyDown);
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(74, 60);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(68, 13);
            this.labelControl16.TabIndex = 26;
            this.labelControl16.Text = "Next Service :";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(71, 90);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(71, 13);
            this.labelControl17.TabIndex = 27;
            this.labelControl17.Text = "Insurance No :";
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.simpleButton7);
            this.xtraTabPage7.Controls.Add(this.simpleButton1);
            this.xtraTabPage7.Controls.Add(this.labelControl2);
            this.xtraTabPage7.Controls.Add(this.labelControl4);
            this.xtraTabPage7.Controls.Add(this.txtAdress2);
            this.xtraTabPage7.Controls.Add(this.txtName);
            this.xtraTabPage7.Controls.Add(this.txtAdress1);
            this.xtraTabPage7.Controls.Add(this.labelControl1);
            this.xtraTabPage7.Controls.Add(this.labelControl7);
            this.xtraTabPage7.Controls.Add(this.txtContact);
            this.xtraTabPage7.Controls.Add(this.txtNIC);
            this.xtraTabPage7.Controls.Add(this.labelControl3);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(469, 285);
            this.xtraTabPage7.Text = "Owner Deatils";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(184, 209);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(75, 23);
            this.simpleButton7.TabIndex = 37;
            this.simpleButton7.Text = "Save";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(85, 13);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 36;
            this.simpleButton1.Text = "Clear";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(28, 71);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(34, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Name :";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(7, 123);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(55, 13);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Address 2 :";
            // 
            // txtAdress2
            // 
            this.txtAdress2.Location = new System.Drawing.Point(66, 120);
            this.txtAdress2.Name = "txtAdress2";
            this.txtAdress2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAdress2.Size = new System.Drawing.Size(383, 20);
            this.txtAdress2.TabIndex = 6;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(66, 68);
            this.txtName.Name = "txtName";
            this.txtName.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.Size = new System.Drawing.Size(383, 20);
            this.txtName.TabIndex = 8;
            // 
            // txtAdress1
            // 
            this.txtAdress1.Location = new System.Drawing.Point(66, 94);
            this.txtAdress1.Name = "txtAdress1";
            this.txtAdress1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAdress1.Size = new System.Drawing.Size(383, 20);
            this.txtAdress1.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(37, 45);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(25, 13);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "NIC :";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(17, 153);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(45, 13);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "Contact :";
            // 
            // txtContact
            // 
            this.txtContact.Location = new System.Drawing.Point(66, 146);
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(193, 20);
            this.txtContact.TabIndex = 6;
            // 
            // txtNIC
            // 
            this.txtNIC.Location = new System.Drawing.Point(66, 42);
            this.txtNIC.Name = "txtNIC";
            this.txtNIC.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNIC.Size = new System.Drawing.Size(193, 20);
            this.txtNIC.TabIndex = 9;
            this.txtNIC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNIC_KeyDown);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(7, 97);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(55, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Address 1 :";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(545, 301);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(36, 22);
            this.simpleButton5.TabIndex = 47;
            this.simpleButton5.Text = "Set";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(545, 275);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(36, 22);
            this.simpleButton4.TabIndex = 46;
            this.simpleButton4.Text = "Set";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(545, 249);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(36, 22);
            this.simpleButton3.TabIndex = 45;
            this.simpleButton3.Text = "Set";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(545, 223);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(36, 22);
            this.simpleButton2.TabIndex = 44;
            this.simpleButton2.Text = "Set";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(233, 310);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(49, 13);
            this.labelControl22.TabIndex = 42;
            this.labelControl22.Text = "Picture 4 :";
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(297, 307);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(242, 20);
            this.textEdit4.TabIndex = 43;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(233, 284);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(49, 13);
            this.labelControl21.TabIndex = 40;
            this.labelControl21.Text = "Picture 3 :";
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(297, 281);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(242, 20);
            this.textEdit3.TabIndex = 41;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(233, 258);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(49, 13);
            this.labelControl20.TabIndex = 38;
            this.labelControl20.Text = "Picture 2 :";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(297, 255);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(242, 20);
            this.textEdit2.TabIndex = 39;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(233, 232);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(49, 13);
            this.labelControl19.TabIndex = 36;
            this.labelControl19.Text = "Picture 1 :";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(297, 229);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(242, 20);
            this.textEdit1.TabIndex = 37;
            // 
            // lstVhclFull
            // 
            this.lstVhclFull.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8});
            this.lstVhclFull.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstVhclFull.FullRowSelect = true;
            this.lstVhclFull.Location = new System.Drawing.Point(5, 20);
            this.lstVhclFull.MultiSelect = false;
            this.lstVhclFull.Name = "lstVhclFull";
            this.lstVhclFull.Size = new System.Drawing.Size(216, 313);
            this.lstVhclFull.TabIndex = 18;
            this.lstVhclFull.UseCompatibleStateImageBehavior = false;
            this.lstVhclFull.View = System.Windows.Forms.View.Tile;
            this.lstVhclFull.SelectedIndexChanged += new System.EventHandler(this.lstVhclFull_SelectedIndexChanged);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Vehicle No";
            this.columnHeader7.Width = 100;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Type";
            this.columnHeader8.Width = 100;
            // 
            // lblVhclID
            // 
            this.lblVhclID.Appearance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVhclID.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblVhclID.Appearance.Options.UseFont = true;
            this.lblVhclID.Appearance.Options.UseForeColor = true;
            this.lblVhclID.Location = new System.Drawing.Point(703, 203);
            this.lblVhclID.Name = "lblVhclID";
            this.lblVhclID.Size = new System.Drawing.Size(54, 15);
            this.lblVhclID.TabIndex = 16;
            this.lblVhclID.Text = "Vehicle ID";
            // 
            // btnVhclClear
            // 
            this.btnVhclClear.Location = new System.Drawing.Point(231, 27);
            this.btnVhclClear.Name = "btnVhclClear";
            this.btnVhclClear.Size = new System.Drawing.Size(62, 23);
            this.btnVhclClear.TabIndex = 35;
            this.btnVhclClear.Text = "Clear";
            this.btnVhclClear.Click += new System.EventHandler(this.btnVhclClear_Click);
            // 
            // btnDelVhcl
            // 
            this.btnDelVhcl.Location = new System.Drawing.Point(695, 227);
            this.btnDelVhcl.Name = "btnDelVhcl";
            this.btnDelVhcl.Size = new System.Drawing.Size(75, 23);
            this.btnDelVhcl.TabIndex = 34;
            this.btnDelVhcl.Text = "Delete";
            this.btnDelVhcl.Click += new System.EventHandler(this.btnDelVhcl_Click);
            // 
            // btnUpdateVhcl
            // 
            this.btnUpdateVhcl.Location = new System.Drawing.Point(695, 266);
            this.btnUpdateVhcl.Name = "btnUpdateVhcl";
            this.btnUpdateVhcl.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateVhcl.TabIndex = 33;
            this.btnUpdateVhcl.Text = "Update";
            this.btnUpdateVhcl.Click += new System.EventHandler(this.btnUpdateVhcl_Click);
            // 
            // dtpDelDate
            // 
            this.dtpDelDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDelDate.Location = new System.Drawing.Point(437, 182);
            this.dtpDelDate.Name = "dtpDelDate";
            this.dtpDelDate.Size = new System.Drawing.Size(93, 21);
            this.dtpDelDate.TabIndex = 32;
            // 
            // btnVhclSave
            // 
            this.btnVhclSave.Location = new System.Drawing.Point(695, 304);
            this.btnVhclSave.Name = "btnVhclSave";
            this.btnVhclSave.Size = new System.Drawing.Size(75, 23);
            this.btnVhclSave.TabIndex = 13;
            this.btnVhclSave.Text = "Save";
            this.btnVhclSave.Click += new System.EventHandler(this.btnVhclSave_Click);
            // 
            // picVhcl
            // 
            this.picVhcl.Cursor = System.Windows.Forms.Cursors.Default;
            this.picVhcl.EditValue = global::CarRentPro.Properties.Resources.images;
            this.picVhcl.Location = new System.Drawing.Point(94, 62);
            this.picVhcl.Name = "picVhcl";
            this.picVhcl.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("picVhcl.Properties.InitialImage")));
            this.picVhcl.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picVhcl.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picVhcl.Properties.ZoomAccelerationFactor = 1D;
            this.picVhcl.Size = new System.Drawing.Size(127, 105);
            this.picVhcl.TabIndex = 31;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(338, 185);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(78, 13);
            this.labelControl5.TabIndex = 20;
            this.labelControl5.Text = "Delivered Date :";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(301, 88);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(31, 13);
            this.labelControl11.TabIndex = 21;
            this.labelControl11.Text = "Type :";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(312, 35);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(20, 13);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "No :";
            // 
            // txtVhclNo
            // 
            this.txtVhclNo.Location = new System.Drawing.Point(337, 32);
            this.txtVhclNo.Name = "txtVhclNo";
            this.txtVhclNo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNo.TabIndex = 19;
            this.txtVhclNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVhclNo_KeyDown);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(300, 62);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(32, 13);
            this.labelControl10.TabIndex = 13;
            this.labelControl10.Text = "Make :";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(297, 113);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(35, 13);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Model :";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(274, 139);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(58, 13);
            this.labelControl9.TabIndex = 11;
            this.labelControl9.Text = "Made Year :";
            // 
            // txtVhclMYeay
            // 
            this.txtVhclMYeay.Location = new System.Drawing.Point(338, 136);
            this.txtVhclMYeay.Name = "txtVhclMYeay";
            this.txtVhclMYeay.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclMYeay.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMYeay.TabIndex = 16;
            // 
            // txtVhclModel
            // 
            this.txtVhclModel.Location = new System.Drawing.Point(338, 110);
            this.txtVhclModel.Name = "txtVhclModel";
            this.txtVhclModel.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclModel.Size = new System.Drawing.Size(193, 20);
            this.txtVhclModel.TabIndex = 17;
            // 
            // txtVhclMake
            // 
            this.txtVhclMake.Location = new System.Drawing.Point(338, 59);
            this.txtVhclMake.Name = "txtVhclMake";
            this.txtVhclMake.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclMake.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMake.TabIndex = 18;
            // 
            // txtVhclType
            // 
            this.txtVhclType.Location = new System.Drawing.Point(338, 85);
            this.txtVhclType.Name = "txtVhclType";
            this.txtVhclType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtVhclType.Properties.Items.AddRange(new object[] {
            "Car",
            "Van",
            "Bike"});
            this.txtVhclType.Size = new System.Drawing.Size(193, 20);
            this.txtVhclType.TabIndex = 22;
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(610, 163);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(62, 23);
            this.simpleButton8.TabIndex = 57;
            this.simpleButton8.Text = "Test";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnSupDelete);
            this.groupControl1.Controls.Add(this.lstSppliers);
            this.groupControl1.Controls.Add(this.lblSupId);
            this.groupControl1.Controls.Add(this.btnSupUpdate);
            this.groupControl1.Controls.Add(this.btnSupSave);
            this.groupControl1.Controls.Add(this.lstVhcl);
            this.groupControl1.Location = new System.Drawing.Point(84, 9);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(712, 255);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Supplyer Deatils";
            // 
            // btnSupDelete
            // 
            this.btnSupDelete.Location = new System.Drawing.Point(659, 216);
            this.btnSupDelete.Name = "btnSupDelete";
            this.btnSupDelete.Size = new System.Drawing.Size(75, 23);
            this.btnSupDelete.TabIndex = 17;
            this.btnSupDelete.Text = "Delete";
            this.btnSupDelete.Click += new System.EventHandler(this.btnSupDelete_Click);
            // 
            // lstSppliers
            // 
            this.lstSppliers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
            this.lstSppliers.Dock = System.Windows.Forms.DockStyle.Left;
            this.lstSppliers.FullRowSelect = true;
            this.lstSppliers.Location = new System.Drawing.Point(2, 20);
            this.lstSppliers.Name = "lstSppliers";
            this.lstSppliers.Size = new System.Drawing.Size(396, 233);
            this.lstSppliers.TabIndex = 16;
            this.lstSppliers.UseCompatibleStateImageBehavior = false;
            this.lstSppliers.View = System.Windows.Forms.View.Details;
            this.lstSppliers.SelectedIndexChanged += new System.EventHandler(this.lstSppliers_SelectedIndexChanged);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Supplier NIC";
            this.columnHeader5.Width = 121;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Name";
            this.columnHeader6.Width = 263;
            // 
            // lblSupId
            // 
            this.lblSupId.Location = new System.Drawing.Point(798, 44);
            this.lblSupId.Name = "lblSupId";
            this.lblSupId.Size = new System.Drawing.Size(52, 13);
            this.lblSupId.TabIndex = 15;
            this.lblSupId.Text = "Supplier ID";
            // 
            // btnSupUpdate
            // 
            this.btnSupUpdate.Location = new System.Drawing.Point(568, 216);
            this.btnSupUpdate.Name = "btnSupUpdate";
            this.btnSupUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnSupUpdate.TabIndex = 13;
            this.btnSupUpdate.Text = "Update";
            this.btnSupUpdate.Click += new System.EventHandler(this.btnSupUpdate_Click);
            // 
            // btnSupSave
            // 
            this.btnSupSave.Location = new System.Drawing.Point(477, 216);
            this.btnSupSave.Name = "btnSupSave";
            this.btnSupSave.Size = new System.Drawing.Size(75, 23);
            this.btnSupSave.TabIndex = 12;
            this.btnSupSave.Text = "Save";
            this.btnSupSave.Click += new System.EventHandler(this.btnSupSave_Click);
            // 
            // lstVhcl
            // 
            this.lstVhcl.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lstVhcl.Dock = System.Windows.Forms.DockStyle.Right;
            this.lstVhcl.Location = new System.Drawing.Point(314, 20);
            this.lstVhcl.Name = "lstVhcl";
            this.lstVhcl.Size = new System.Drawing.Size(396, 233);
            this.lstVhcl.TabIndex = 14;
            this.lstVhcl.UseCompatibleStateImageBehavior = false;
            this.lstVhcl.View = System.Windows.Forms.View.Details;
            this.lstVhcl.SelectedIndexChanged += new System.EventHandler(this.lstVhcl_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Vehicle No";
            this.columnHeader1.Width = 82;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Make";
            this.columnHeader2.Width = 75;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Model";
            this.columnHeader3.Width = 71;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Delivered Date";
            this.columnHeader4.Width = 137;
            // 
            // carrentproDataSet1
            // 
            this.carrentproDataSet1.DataSetName = "carrentproDataSet";
            this.carrentproDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // carrentproDataSet2
            // 
            this.carrentproDataSet2.DataSetName = "carrentproDataSet";
            this.carrentproDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // appointmentsTableAdapter1
            // 
            this.appointmentsTableAdapter1.ClearBeforeFill = true;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(112, 188);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(82, 20);
            this.textEdit5.TabIndex = 46;
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(7, 191);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(105, 13);
            this.labelControl29.TabIndex = 45;
            this.labelControl29.Text = "Night charge (Rs/km):";
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(200, 78);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(82, 20);
            this.textEdit6.TabIndex = 49;
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(223, 59);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(41, 13);
            this.labelControl28.TabIndex = 47;
            this.labelControl28.Text = "Per Day ";
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(200, 104);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(82, 20);
            this.textEdit8.TabIndex = 48;
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(135, 32);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(38, 13);
            this.labelControl30.TabIndex = 50;
            this.labelControl30.Text = "Rate 01";
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(223, 32);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(38, 13);
            this.labelControl31.TabIndex = 51;
            this.labelControl31.Text = "Rate 02";
            // 
            // FrmVehicles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 603);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FrmVehicles";
            this.Text = "Vehicles & Supplyers";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmVehicles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageComboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHirePerKm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraChergePerKm.Properties)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclLicNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclInsNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNxtSrvice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclCurOdo.Properties)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            this.xtraTabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picVhcl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.carrentproDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.carrentproDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit txtAdress2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtAdress1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNIC;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtContact;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtVhclNo;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtVhclMYeay;
        private DevExpress.XtraEditors.TextEdit txtVhclModel;
        private DevExpress.XtraEditors.TextEdit txtVhclMake;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.TextEdit txtVhclNxtSrvice;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtVhclCurOdo;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.TextEdit txtDayLimit;
        private DevExpress.XtraEditors.TextEdit txtVhclLicNo;
        private DevExpress.XtraEditors.TextEdit txtVhclInsNo;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.PictureEdit picVhcl;
        private DevExpress.XtraEditors.SimpleButton btnSupSave;
        private DevExpress.XtraEditors.SimpleButton btnVhclSave;
        private System.Windows.Forms.DateTimePicker dtpDelDate;
        private System.Windows.Forms.DateTimePicker dtpLicExpDate;
        private System.Windows.Forms.DateTimePicker dtpInsExpDate;
        private DevExpress.XtraEditors.SimpleButton btnVhclClear;
        private DevExpress.XtraEditors.SimpleButton btnDelVhcl;
        private DevExpress.XtraEditors.SimpleButton btnUpdateVhcl;
        private DevExpress.XtraEditors.SimpleButton btnSupUpdate;
        private carrentproDataSet carrentproDataSet1;
        private System.Windows.Forms.ListView lstVhcl;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private DevExpress.XtraEditors.LabelControl lblVhclID;
        private DevExpress.XtraEditors.LabelControl lblSupId;
        private System.Windows.Forms.ListView lstSppliers;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private DevExpress.XtraEditors.SimpleButton btnSupDelete;
        private System.Windows.Forms.ListView lstVhclFull;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private DevExpress.XtraEditors.ComboBoxEdit txtVhclType;
        private DevExpress.XtraEditors.SimpleButton btnRateSave;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private DevExpress.XtraEditors.Controls.ImageSlider imageSlider1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private DevExpress.XtraEditors.ImageComboBoxEdit imageComboBoxEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit txtHirePerKm;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtExtraChergePerKm;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private carrentproDataSet carrentproDataSet2;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private carrentproDataSetTableAdapters.appointmentsTableAdapter appointmentsTableAdapter1;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.TextEdit txtPDay;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl29;
    }
}