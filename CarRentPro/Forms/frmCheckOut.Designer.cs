﻿namespace CarRentPro.Forms
{
    partial class frmCheckOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCheckOut));
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lblPaid = new DevExpress.XtraEditors.LabelControl();
            this.lblDue = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.lblGrandTotal = new DevExpress.XtraEditors.LabelControl();
            this.btnPay = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.lbldecor = new DevExpress.XtraEditors.LabelControl();
            this.lblDriverCharge = new DevExpress.XtraEditors.LabelControl();
            this.lblRental = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtExKmCharge = new DevExpress.XtraEditors.TextEdit();
            this.txtShortTime = new DevExpress.XtraEditors.TextEdit();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.cGauge1 = new DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge();
            this.arcScaleBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleNeedleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleComponent2 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.lblPetrolValue = new DevExpress.XtraEditors.LabelControl();
            this.txtpetrolcharg = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.txtHrs = new DevExpress.XtraEditors.TextEdit();
            this.txtDays = new DevExpress.XtraEditors.TextEdit();
            this.txtExCharges = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txtCurODO = new DevExpress.XtraEditors.TextEdit();
            this.tpEnd = new DevExpress.XtraEditors.TimeEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.tpStart = new DevExpress.XtraEditors.TimeEdit();
            this.cmbDriverList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbVehicleList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txtDisLimit = new DevExpress.XtraEditors.TextEdit();
            this.txtDriverCharges = new DevExpress.XtraEditors.TextEdit();
            this.chkDriver = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtDayRate = new DevExpress.XtraEditors.TextEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtContact2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.AddCst = new DevExpress.XtraEditors.SimpleButton();
            this.PicCustomer = new DevExpress.XtraEditors.PictureEdit();
            this.lblCstID = new DevExpress.XtraEditors.LabelControl();
            this.lblIsBlackListed = new DevExpress.XtraEditors.LabelControl();
            this.dtpLicExp = new System.Windows.Forms.DateTimePicker();
            this.txtContact1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtLIcNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtAddress1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtFName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtNIC = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.dtpLicExpDate = new System.Windows.Forms.DateTimePicker();
            this.dtpInsExpDate = new System.Windows.Forms.DateTimePicker();
            this.txtVhclLicNo = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclInsNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclNxtSrvice = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclCurOdo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lblVhclID = new DevExpress.XtraEditors.LabelControl();
            this.imageSlider1 = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.dtpDelDate = new System.Windows.Forms.DateTimePicker();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclNo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtVhclMYeay = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclModel = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclMake = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.lstPayments = new System.Windows.Forms.ListView();
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtExKmCharge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShortTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpetrolcharg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHrs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExCharges.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurODO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDriverList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVehicleList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisLimit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverCharges.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDriver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCustomer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLIcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclLicNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclInsNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNxtSrvice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclCurOdo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(668, 600);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(87, 30);
            this.simpleButton1.TabIndex = 59;
            this.simpleButton1.Text = "Add";
            // 
            // lblPaid
            // 
            this.lblPaid.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaid.Appearance.Options.UseFont = true;
            this.lblPaid.Appearance.Options.UseTextOptions = true;
            this.lblPaid.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblPaid.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPaid.Location = new System.Drawing.Point(1147, 563);
            this.lblPaid.Name = "lblPaid";
            this.lblPaid.Size = new System.Drawing.Size(112, 19);
            this.lblPaid.TabIndex = 56;
            this.lblPaid.Text = "0.00";
            // 
            // lblDue
            // 
            this.lblDue.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDue.Appearance.Options.UseFont = true;
            this.lblDue.Appearance.Options.UseTextOptions = true;
            this.lblDue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDue.Location = new System.Drawing.Point(1147, 590);
            this.lblDue.Name = "lblDue";
            this.lblDue.Size = new System.Drawing.Size(112, 19);
            this.lblDue.TabIndex = 55;
            this.lblDue.Text = "0.00";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.Options.UseFont = true;
            this.labelControl29.Location = new System.Drawing.Point(1004, 590);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(109, 19);
            this.labelControl29.TabIndex = 54;
            this.labelControl29.Text = "Due Payment";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.Options.UseFont = true;
            this.labelControl28.Location = new System.Drawing.Point(1004, 563);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(36, 19);
            this.labelControl28.TabIndex = 53;
            this.labelControl28.Text = "Paid";
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.Options.UseFont = true;
            this.labelControl25.Location = new System.Drawing.Point(1004, 531);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(97, 19);
            this.labelControl25.TabIndex = 52;
            this.labelControl25.Text = "Grand Total";
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = global::CarRentPro.Properties.Resources.save_32x32;
            this.btnSave.Location = new System.Drawing.Point(849, 590);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(101, 44);
            this.btnSave.TabIndex = 58;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrandTotal.Appearance.Options.UseFont = true;
            this.lblGrandTotal.Appearance.Options.UseTextOptions = true;
            this.lblGrandTotal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblGrandTotal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblGrandTotal.Location = new System.Drawing.Point(1147, 531);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(112, 19);
            this.lblGrandTotal.TabIndex = 51;
            this.lblGrandTotal.Text = "0.00";
            // 
            // btnPay
            // 
            this.btnPay.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPay.ImageOptions.Image")));
            this.btnPay.Location = new System.Drawing.Point(849, 531);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(101, 44);
            this.btnPay.TabIndex = 57;
            this.btnPay.Text = "Pay Now";
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click_1);
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.lbldecor);
            this.groupControl5.Controls.Add(this.lblDriverCharge);
            this.groupControl5.Controls.Add(this.lblRental);
            this.groupControl5.Controls.Add(this.labelControl12);
            this.groupControl5.Controls.Add(this.txtExKmCharge);
            this.groupControl5.Controls.Add(this.txtShortTime);
            this.groupControl5.Controls.Add(this.gaugeControl1);
            this.groupControl5.Controls.Add(this.lblPetrolValue);
            this.groupControl5.Controls.Add(this.txtpetrolcharg);
            this.groupControl5.Controls.Add(this.checkEdit5);
            this.groupControl5.Controls.Add(this.labelControl45);
            this.groupControl5.Controls.Add(this.txtHrs);
            this.groupControl5.Controls.Add(this.txtDays);
            this.groupControl5.Controls.Add(this.txtExCharges);
            this.groupControl5.Controls.Add(this.labelControl31);
            this.groupControl5.Controls.Add(this.txtCurODO);
            this.groupControl5.Controls.Add(this.tpEnd);
            this.groupControl5.Controls.Add(this.checkEdit1);
            this.groupControl5.Controls.Add(this.labelControl32);
            this.groupControl5.Controls.Add(this.tpStart);
            this.groupControl5.Controls.Add(this.cmbDriverList);
            this.groupControl5.Controls.Add(this.cmbVehicleList);
            this.groupControl5.Controls.Add(this.dtpEnd);
            this.groupControl5.Controls.Add(this.dtpStart);
            this.groupControl5.Controls.Add(this.labelControl27);
            this.groupControl5.Controls.Add(this.txtDisLimit);
            this.groupControl5.Controls.Add(this.txtDriverCharges);
            this.groupControl5.Controls.Add(this.chkDriver);
            this.groupControl5.Controls.Add(this.labelControl24);
            this.groupControl5.Controls.Add(this.txtDayRate);
            this.groupControl5.Controls.Add(this.labelControl23);
            this.groupControl5.Controls.Add(this.labelControl22);
            this.groupControl5.Controls.Add(this.labelControl21);
            this.groupControl5.Controls.Add(this.dateNavigator1);
            this.groupControl5.Location = new System.Drawing.Point(661, 1);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(619, 524);
            this.groupControl5.TabIndex = 50;
            this.groupControl5.Text = "Rates && Calculation";
            // 
            // lbldecor
            // 
            this.lbldecor.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldecor.Appearance.Options.UseFont = true;
            this.lbldecor.Appearance.Options.UseTextOptions = true;
            this.lbldecor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbldecor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbldecor.Location = new System.Drawing.Point(516, 437);
            this.lbldecor.Name = "lbldecor";
            this.lbldecor.Size = new System.Drawing.Size(82, 19);
            this.lbldecor.TabIndex = 63;
            this.lbldecor.Text = "0.00";
            this.lbldecor.TextChanged += new System.EventHandler(this.lbldecor_TextChanged);
            // 
            // lblDriverCharge
            // 
            this.lblDriverCharge.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverCharge.Appearance.Options.UseFont = true;
            this.lblDriverCharge.Appearance.Options.UseTextOptions = true;
            this.lblDriverCharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDriverCharge.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDriverCharge.Location = new System.Drawing.Point(516, 400);
            this.lblDriverCharge.Name = "lblDriverCharge";
            this.lblDriverCharge.Size = new System.Drawing.Size(82, 19);
            this.lblDriverCharge.TabIndex = 62;
            this.lblDriverCharge.Text = "0.00";
            this.lblDriverCharge.TextChanged += new System.EventHandler(this.lblDriverCharge_TextChanged);
            // 
            // lblRental
            // 
            this.lblRental.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRental.Appearance.Options.UseFont = true;
            this.lblRental.Appearance.Options.UseTextOptions = true;
            this.lblRental.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblRental.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblRental.Location = new System.Drawing.Point(516, 345);
            this.lblRental.Name = "lblRental";
            this.lblRental.Size = new System.Drawing.Size(82, 19);
            this.lblRental.TabIndex = 60;
            this.lblRental.Text = "0.00";
            this.lblRental.TextChanged += new System.EventHandler(this.lblRental_TextChanged);
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(194, 489);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(99, 13);
            this.labelControl12.TabIndex = 61;
            this.labelControl12.Text = "Charge per Extra km";
            // 
            // txtExKmCharge
            // 
            this.txtExKmCharge.Location = new System.Drawing.Point(315, 486);
            this.txtExKmCharge.Name = "txtExKmCharge";
            this.txtExKmCharge.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtExKmCharge.Size = new System.Drawing.Size(107, 20);
            this.txtExKmCharge.TabIndex = 60;
            // 
            // txtShortTime
            // 
            this.txtShortTime.Location = new System.Drawing.Point(475, 106);
            this.txtShortTime.Name = "txtShortTime";
            this.txtShortTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtShortTime.Size = new System.Drawing.Size(105, 20);
            this.txtShortTime.TabIndex = 59;
            this.txtShortTime.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShortTime_KeyDown);
            this.txtShortTime.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtShortTime_KeyUp);
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.cGauge1});
            this.gaugeControl1.Location = new System.Drawing.Point(8, 339);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(147, 152);
            this.gaugeControl1.TabIndex = 58;
            this.gaugeControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gaugeControl1_MouseMove_1);
            // 
            // cGauge1
            // 
            this.cGauge1.BackgroundLayers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent[] {
            this.arcScaleBackgroundLayerComponent1});
            this.cGauge1.Bounds = new System.Drawing.Rectangle(6, 6, 135, 140);
            this.cGauge1.Name = "cGauge1";
            this.cGauge1.Needles.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent[] {
            this.arcScaleNeedleComponent1});
            this.cGauge1.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent[] {
            this.arcScaleComponent1,
            this.arcScaleComponent2});
            // 
            // arcScaleBackgroundLayerComponent1
            // 
            this.arcScaleBackgroundLayerComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleBackgroundLayerComponent1.Name = "bg1";
            this.arcScaleBackgroundLayerComponent1.ScaleCenterPos = new DevExpress.XtraGauges.Core.Base.PointF2D(0.85F, 0.85F);
            this.arcScaleBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularQuarter_Style11Left;
            this.arcScaleBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // arcScaleComponent1
            // 
            this.arcScaleComponent1.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 14F);
            this.arcScaleComponent1.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(212F, 212F);
            this.arcScaleComponent1.EndAngle = -90F;
            this.arcScaleComponent1.MajorTickCount = 3;
            this.arcScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent1.MajorTickmark.ShapeOffset = -6F;
            this.arcScaleComponent1.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.6F, 1.6F);
            this.arcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_4;
            this.arcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent1.MaxValue = 10F;
            this.arcScaleComponent1.MinorTickCount = 4;
            this.arcScaleComponent1.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.6F, 1.6F);
            this.arcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_3;
            this.arcScaleComponent1.Name = "scale1";
            this.arcScaleComponent1.RadiusX = 176F;
            this.arcScaleComponent1.RadiusY = 176F;
            this.arcScaleComponent1.StartAngle = -180F;
            this.arcScaleComponent1.Value = 10F;
            // 
            // arcScaleNeedleComponent1
            // 
            this.arcScaleNeedleComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleNeedleComponent1.EndOffset = -12F;
            this.arcScaleNeedleComponent1.Name = "needle1";
            this.arcScaleNeedleComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_Style11;
            this.arcScaleNeedleComponent1.StartOffset = -17.5F;
            this.arcScaleNeedleComponent1.ZOrder = -50;
            // 
            // arcScaleComponent2
            // 
            this.arcScaleComponent2.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent2.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.arcScaleComponent2.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent2.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(212F, 212F);
            this.arcScaleComponent2.EndAngle = -90F;
            this.arcScaleComponent2.MajorTickCount = 4;
            this.arcScaleComponent2.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent2.MajorTickmark.ShapeOffset = -3F;
            this.arcScaleComponent2.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_2;
            this.arcScaleComponent2.MajorTickmark.TextOffset = -20F;
            this.arcScaleComponent2.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent2.MaxValue = 500F;
            this.arcScaleComponent2.MinorTickCount = 4;
            this.arcScaleComponent2.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style11_1;
            this.arcScaleComponent2.MinValue = 200F;
            this.arcScaleComponent2.Name = "scale2";
            this.arcScaleComponent2.RadiusX = 120F;
            this.arcScaleComponent2.RadiusY = 120F;
            this.arcScaleComponent2.StartAngle = -180F;
            this.arcScaleComponent2.Value = 200F;
            this.arcScaleComponent2.ZOrder = -1;
            // 
            // lblPetrolValue
            // 
            this.lblPetrolValue.Location = new System.Drawing.Point(428, 463);
            this.lblPetrolValue.Name = "lblPetrolValue";
            this.lblPetrolValue.Size = new System.Drawing.Size(11, 13);
            this.lblPetrolValue.TabIndex = 57;
            this.lblPetrolValue.Text = "%";
            // 
            // txtpetrolcharg
            // 
            this.txtpetrolcharg.Location = new System.Drawing.Point(317, 460);
            this.txtpetrolcharg.Name = "txtpetrolcharg";
            this.txtpetrolcharg.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtpetrolcharg.Size = new System.Drawing.Size(105, 20);
            this.txtpetrolcharg.TabIndex = 56;
            this.txtpetrolcharg.Leave += new System.EventHandler(this.txtpetrolcharg_Leave);
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(170, 460);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "Fuel Level Amount (Rs.)";
            this.checkEdit5.Size = new System.Drawing.Size(141, 19);
            this.checkEdit5.TabIndex = 55;
            this.checkEdit5.CheckedChanged += new System.EventHandler(this.checkEdit5_CheckedChanged);
            // 
            // labelControl45
            // 
            this.labelControl45.Location = new System.Drawing.Point(536, 216);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(28, 13);
            this.labelControl45.TabIndex = 54;
            this.labelControl45.Text = "Hours";
            // 
            // txtHrs
            // 
            this.txtHrs.Location = new System.Drawing.Point(532, 232);
            this.txtHrs.Name = "txtHrs";
            this.txtHrs.Size = new System.Drawing.Size(48, 20);
            this.txtHrs.TabIndex = 53;
            // 
            // txtDays
            // 
            this.txtDays.Location = new System.Drawing.Point(475, 232);
            this.txtDays.Name = "txtDays";
            this.txtDays.Size = new System.Drawing.Size(49, 20);
            this.txtDays.TabIndex = 52;
            // 
            // txtExCharges
            // 
            this.txtExCharges.Location = new System.Drawing.Point(317, 436);
            this.txtExCharges.Name = "txtExCharges";
            this.txtExCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtExCharges.Size = new System.Drawing.Size(105, 20);
            this.txtExCharges.TabIndex = 50;
            this.txtExCharges.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtExCharges_KeyDown);
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(11, 301);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(112, 13);
            this.labelControl31.TabIndex = 48;
            this.labelControl31.Text = "Current ODO Reading :";
            // 
            // txtCurODO
            // 
            this.txtCurODO.Location = new System.Drawing.Point(129, 298);
            this.txtCurODO.Name = "txtCurODO";
            this.txtCurODO.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCurODO.Size = new System.Drawing.Size(103, 20);
            this.txtCurODO.TabIndex = 47;
            this.txtCurODO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCurODO_KeyDown);
            // 
            // tpEnd
            // 
            this.tpEnd.EditValue = new System.DateTime(2017, 12, 23, 0, 0, 0, 0);
            this.tpEnd.Location = new System.Drawing.Point(475, 177);
            this.tpEnd.Name = "tpEnd";
            this.tpEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tpEnd.Properties.Mask.EditMask = "t";
            this.tpEnd.Properties.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.TouchUI;
            this.tpEnd.Size = new System.Drawing.Size(105, 20);
            this.tpEnd.TabIndex = 45;
            this.tpEnd.EditValueChanged += new System.EventHandler(this.tpEnd_EditValueChanged);
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(170, 435);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Decor Charges (Rs.)";
            this.checkEdit1.Size = new System.Drawing.Size(123, 19);
            this.checkEdit1.TabIndex = 44;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(11, 272);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(56, 13);
            this.labelControl32.TabIndex = 25;
            this.labelControl32.Text = "Vehicle No :";
            // 
            // tpStart
            // 
            this.tpStart.EditValue = new System.DateTime(2017, 12, 23, 0, 0, 0, 0);
            this.tpStart.Location = new System.Drawing.Point(475, 73);
            this.tpStart.Name = "tpStart";
            this.tpStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tpStart.Properties.Mask.EditMask = "t";
            this.tpStart.Properties.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.TouchUI;
            this.tpStart.Size = new System.Drawing.Size(105, 20);
            this.tpStart.TabIndex = 43;
            // 
            // cmbDriverList
            // 
            this.cmbDriverList.Location = new System.Drawing.Point(317, 373);
            this.cmbDriverList.Name = "cmbDriverList";
            this.cmbDriverList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDriverList.Size = new System.Drawing.Size(193, 20);
            this.cmbDriverList.TabIndex = 24;
            this.cmbDriverList.SelectedIndexChanged += new System.EventHandler(this.cmbDriverList_SelectedIndexChanged);
            // 
            // cmbVehicleList
            // 
            this.cmbVehicleList.Location = new System.Drawing.Point(73, 269);
            this.cmbVehicleList.Name = "cmbVehicleList";
            this.cmbVehicleList.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehicleList.Properties.Appearance.Options.UseFont = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVehicleList.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.cmbVehicleList.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cmbVehicleList.Properties.AutoComplete = false;
            this.cmbVehicleList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbVehicleList.Properties.DropDownRows = 20;
            this.cmbVehicleList.Properties.Sorted = true;
            this.cmbVehicleList.Size = new System.Drawing.Size(159, 20);
            this.cmbVehicleList.TabIndex = 26;
            this.cmbVehicleList.SelectedIndexChanged += new System.EventHandler(this.cmbVehicleList_SelectedIndexChanged);
            // 
            // dtpEnd
            // 
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEnd.Location = new System.Drawing.Point(475, 150);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(105, 21);
            this.dtpEnd.TabIndex = 38;
            this.dtpEnd.ValueChanged += new System.EventHandler(this.dtpEnd_ValueChanged);
            // 
            // dtpStart
            // 
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStart.Location = new System.Drawing.Point(475, 50);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(105, 21);
            this.dtpStart.TabIndex = 37;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(255, 301);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(45, 13);
            this.labelControl27.TabIndex = 20;
            this.labelControl27.Text = "Limit (km)";
            // 
            // txtDisLimit
            // 
            this.txtDisLimit.Location = new System.Drawing.Point(317, 298);
            this.txtDisLimit.Name = "txtDisLimit";
            this.txtDisLimit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDisLimit.Size = new System.Drawing.Size(92, 20);
            this.txtDisLimit.TabIndex = 19;
            this.txtDisLimit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDisLimit_KeyDown);
            // 
            // txtDriverCharges
            // 
            this.txtDriverCharges.Location = new System.Drawing.Point(428, 399);
            this.txtDriverCharges.Name = "txtDriverCharges";
            this.txtDriverCharges.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDriverCharges.Size = new System.Drawing.Size(82, 20);
            this.txtDriverCharges.TabIndex = 15;
            this.txtDriverCharges.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDriverCharges_KeyDown);
            // 
            // chkDriver
            // 
            this.chkDriver.Location = new System.Drawing.Point(170, 373);
            this.chkDriver.Name = "chkDriver";
            this.chkDriver.Properties.Caption = "Driver/ Charge Rate (Rs.)";
            this.chkDriver.Size = new System.Drawing.Size(145, 19);
            this.chkDriver.TabIndex = 14;
            this.chkDriver.CheckedChanged += new System.EventHandler(this.chkDriver_CheckedChanged);
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(259, 354);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(50, 13);
            this.labelControl24.TabIndex = 13;
            this.labelControl24.Text = "Rate (Rs.)";
            // 
            // txtDayRate
            // 
            this.txtDayRate.Location = new System.Drawing.Point(317, 347);
            this.txtDayRate.Name = "txtDayRate";
            this.txtDayRate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDayRate.Size = new System.Drawing.Size(124, 20);
            this.txtDayRate.TabIndex = 12;
            this.txtDayRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDayRate_KeyDown);
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(477, 215);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(24, 13);
            this.labelControl23.TabIndex = 11;
            this.labelControl23.Text = "Days";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(483, 132);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(18, 13);
            this.labelControl22.TabIndex = 10;
            this.labelControl22.Text = "End";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(482, 30);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(24, 13);
            this.labelControl21.TabIndex = 8;
            this.labelControl21.Text = "Start";
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.CalendarAppearance.DayCellSpecial.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.dateNavigator1.CalendarAppearance.DayCellSpecial.Options.UseFont = true;
            this.dateNavigator1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNavigator1.Cursor = System.Windows.Forms.Cursors.Default;
            this.dateNavigator1.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.dateNavigator1.Location = new System.Drawing.Point(8, 23);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.Size = new System.Drawing.Size(453, 232);
            this.dateNavigator1.TabIndex = 30;
            this.dateNavigator1.SelectionChanged += new System.EventHandler(this.dateNavigator1_SelectionChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl35);
            this.groupControl1.Controls.Add(this.txtContact3);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.txtContact2);
            this.groupControl1.Controls.Add(this.labelControl34);
            this.groupControl1.Controls.Add(this.AddCst);
            this.groupControl1.Controls.Add(this.PicCustomer);
            this.groupControl1.Controls.Add(this.lblCstID);
            this.groupControl1.Controls.Add(this.lblIsBlackListed);
            this.groupControl1.Controls.Add(this.dtpLicExp);
            this.groupControl1.Controls.Add(this.txtContact1);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.txtLIcNo);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.txtAddress2);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.txtAddress1);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.txtFName);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.txtNIC);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(3, 1);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(659, 247);
            this.groupControl1.TabIndex = 48;
            this.groupControl1.Text = "Customer Deatils";
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(34, 226);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(57, 13);
            this.labelControl35.TabIndex = 10;
            this.labelControl35.Text = "Contact 03:";
            // 
            // txtContact3
            // 
            this.txtContact3.Location = new System.Drawing.Point(98, 223);
            this.txtContact3.Name = "txtContact3";
            this.txtContact3.Size = new System.Drawing.Size(167, 20);
            this.txtContact3.TabIndex = 11;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(271, 150);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(42, 13);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Ex: Date";
            // 
            // txtContact2
            // 
            this.txtContact2.Location = new System.Drawing.Point(98, 197);
            this.txtContact2.Name = "txtContact2";
            this.txtContact2.Size = new System.Drawing.Size(167, 20);
            this.txtContact2.TabIndex = 9;
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(31, 200);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(60, 13);
            this.labelControl34.TabIndex = 8;
            this.labelControl34.Text = "Contact 02 :";
            // 
            // AddCst
            // 
            this.AddCst.Location = new System.Drawing.Point(257, 45);
            this.AddCst.Name = "AddCst";
            this.AddCst.Size = new System.Drawing.Size(87, 23);
            this.AddCst.TabIndex = 7;
            this.AddCst.Text = "Add Customer";
            // 
            // PicCustomer
            // 
            this.PicCustomer.Cursor = System.Windows.Forms.Cursors.Default;
            this.PicCustomer.Location = new System.Drawing.Point(438, 17);
            this.PicCustomer.Name = "PicCustomer";
            this.PicCustomer.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.PicCustomer.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.PicCustomer.Properties.ZoomAccelerationFactor = 1D;
            this.PicCustomer.Size = new System.Drawing.Size(210, 200);
            this.PicCustomer.TabIndex = 6;
            // 
            // lblCstID
            // 
            this.lblCstID.Location = new System.Drawing.Point(352, 50);
            this.lblCstID.Name = "lblCstID";
            this.lblCstID.Size = new System.Drawing.Size(60, 13);
            this.lblCstID.TabIndex = 5;
            this.lblCstID.Text = "Customer ID";
            // 
            // lblIsBlackListed
            // 
            this.lblIsBlackListed.Appearance.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsBlackListed.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblIsBlackListed.Appearance.Options.UseFont = true;
            this.lblIsBlackListed.Appearance.Options.UseForeColor = true;
            this.lblIsBlackListed.Location = new System.Drawing.Point(69, 17);
            this.lblIsBlackListed.Name = "lblIsBlackListed";
            this.lblIsBlackListed.Size = new System.Drawing.Size(140, 29);
            this.lblIsBlackListed.TabIndex = 4;
            this.lblIsBlackListed.Text = " Is Black Listed";
            // 
            // dtpLicExp
            // 
            this.dtpLicExp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLicExp.Location = new System.Drawing.Point(319, 147);
            this.dtpLicExp.Name = "dtpLicExp";
            this.dtpLicExp.Size = new System.Drawing.Size(105, 21);
            this.dtpLicExp.TabIndex = 2;
            // 
            // txtContact1
            // 
            this.txtContact1.Location = new System.Drawing.Point(98, 170);
            this.txtContact1.Name = "txtContact1";
            this.txtContact1.Size = new System.Drawing.Size(167, 20);
            this.txtContact1.TabIndex = 1;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(34, 173);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 13);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Contact 01 :";
            // 
            // txtLIcNo
            // 
            this.txtLIcNo.Location = new System.Drawing.Point(98, 147);
            this.txtLIcNo.Name = "txtLIcNo";
            this.txtLIcNo.Size = new System.Drawing.Size(167, 20);
            this.txtLIcNo.TabIndex = 1;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(17, 146);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(74, 13);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Driving  Lic No :";
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(98, 121);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(334, 20);
            this.txtAddress2.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(39, 124);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(55, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Address 2 :";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(98, 96);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(334, 20);
            this.txtAddress1.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(39, 99);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(55, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Address 1 :";
            // 
            // txtFName
            // 
            this.txtFName.Location = new System.Drawing.Point(98, 71);
            this.txtFName.Name = "txtFName";
            this.txtFName.Size = new System.Drawing.Size(334, 20);
            this.txtFName.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(60, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(34, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Name :";
            // 
            // txtNIC
            // 
            this.txtNIC.Location = new System.Drawing.Point(98, 47);
            this.txtNIC.Name = "txtNIC";
            this.txtNIC.Size = new System.Drawing.Size(139, 20);
            this.txtNIC.TabIndex = 1;
            this.txtNIC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNIC_KeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(69, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(25, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "NIC :";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.xtraTabControl1.Location = new System.Drawing.Point(5, 255);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(657, 354);
            this.xtraTabControl1.TabIndex = 49;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage4,
            this.xtraTabPage6,
            this.xtraTabPage3});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Appearance.PageClient.BackColor = System.Drawing.Color.White;
            this.xtraTabPage2.Appearance.PageClient.Options.UseBackColor = true;
            this.xtraTabPage2.Controls.Add(this.dtpLicExpDate);
            this.xtraTabPage2.Controls.Add(this.dtpInsExpDate);
            this.xtraTabPage2.Controls.Add(this.txtVhclLicNo);
            this.xtraTabPage2.Controls.Add(this.txtVhclInsNo);
            this.xtraTabPage2.Controls.Add(this.labelControl18);
            this.xtraTabPage2.Controls.Add(this.txtVhclNxtSrvice);
            this.xtraTabPage2.Controls.Add(this.labelControl16);
            this.xtraTabPage2.Controls.Add(this.txtVhclCurOdo);
            this.xtraTabPage2.Controls.Add(this.labelControl17);
            this.xtraTabPage2.Controls.Add(this.labelControl19);
            this.xtraTabPage2.Controls.Add(this.lblVhclID);
            this.xtraTabPage2.Controls.Add(this.imageSlider1);
            this.xtraTabPage2.Controls.Add(this.labelControl8);
            this.xtraTabPage2.Controls.Add(this.textEdit18);
            this.xtraTabPage2.Controls.Add(this.pictureEdit4);
            this.xtraTabPage2.Controls.Add(this.pictureEdit3);
            this.xtraTabPage2.Controls.Add(this.pictureEdit2);
            this.xtraTabPage2.Controls.Add(this.pictureEdit1);
            this.xtraTabPage2.Controls.Add(this.dtpDelDate);
            this.xtraTabPage2.Controls.Add(this.labelControl9);
            this.xtraTabPage2.Controls.Add(this.labelControl11);
            this.xtraTabPage2.Controls.Add(this.labelControl10);
            this.xtraTabPage2.Controls.Add(this.txtVhclNo);
            this.xtraTabPage2.Controls.Add(this.labelControl13);
            this.xtraTabPage2.Controls.Add(this.labelControl14);
            this.xtraTabPage2.Controls.Add(this.labelControl15);
            this.xtraTabPage2.Controls.Add(this.txtVhclMYeay);
            this.xtraTabPage2.Controls.Add(this.txtVhclModel);
            this.xtraTabPage2.Controls.Add(this.txtVhclMake);
            this.xtraTabPage2.Controls.Add(this.txtVhclType);
            this.xtraTabPage2.Controls.Add(this.simpleButton8);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(561, 348);
            this.xtraTabPage2.Text = "Vehicle Deatils";
            // 
            // dtpLicExpDate
            // 
            this.dtpLicExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLicExpDate.Location = new System.Drawing.Point(146, 325);
            this.dtpLicExpDate.Name = "dtpLicExpDate";
            this.dtpLicExpDate.Size = new System.Drawing.Size(95, 21);
            this.dtpLicExpDate.TabIndex = 88;
            // 
            // dtpInsExpDate
            // 
            this.dtpInsExpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInsExpDate.Location = new System.Drawing.Point(146, 269);
            this.dtpInsExpDate.Name = "dtpInsExpDate";
            this.dtpInsExpDate.Size = new System.Drawing.Size(95, 21);
            this.dtpInsExpDate.TabIndex = 85;
            // 
            // txtVhclLicNo
            // 
            this.txtVhclLicNo.Location = new System.Drawing.Point(146, 299);
            this.txtVhclLicNo.Name = "txtVhclLicNo";
            this.txtVhclLicNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclLicNo.TabIndex = 87;
            // 
            // txtVhclInsNo
            // 
            this.txtVhclInsNo.Location = new System.Drawing.Point(146, 243);
            this.txtVhclInsNo.Name = "txtVhclInsNo";
            this.txtVhclInsNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclInsNo.TabIndex = 86;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(82, 305);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(58, 13);
            this.labelControl18.TabIndex = 84;
            this.labelControl18.Text = "License No :";
            // 
            // txtVhclNxtSrvice
            // 
            this.txtVhclNxtSrvice.Location = new System.Drawing.Point(146, 213);
            this.txtVhclNxtSrvice.Name = "txtVhclNxtSrvice";
            this.txtVhclNxtSrvice.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNxtSrvice.TabIndex = 83;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(28, 180);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(112, 13);
            this.labelControl16.TabIndex = 79;
            this.labelControl16.Text = "Current ODO Reading :";
            // 
            // txtVhclCurOdo
            // 
            this.txtVhclCurOdo.Location = new System.Drawing.Point(146, 179);
            this.txtVhclCurOdo.Name = "txtVhclCurOdo";
            this.txtVhclCurOdo.Size = new System.Drawing.Size(139, 20);
            this.txtVhclCurOdo.TabIndex = 80;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(72, 216);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(68, 13);
            this.labelControl17.TabIndex = 81;
            this.labelControl17.Text = "Next Service :";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(69, 246);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(71, 13);
            this.labelControl19.TabIndex = 82;
            this.labelControl19.Text = "Insurance No :";
            // 
            // lblVhclID
            // 
            this.lblVhclID.Appearance.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVhclID.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblVhclID.Appearance.Options.UseFont = true;
            this.lblVhclID.Appearance.Options.UseForeColor = true;
            this.lblVhclID.Location = new System.Drawing.Point(476, 174);
            this.lblVhclID.Name = "lblVhclID";
            this.lblVhclID.Size = new System.Drawing.Size(54, 15);
            this.lblVhclID.TabIndex = 78;
            this.lblVhclID.Text = "Vehicle ID";
            // 
            // imageSlider1
            // 
            this.imageSlider1.Cursor = System.Windows.Forms.Cursors.Default;
            this.imageSlider1.Location = new System.Drawing.Point(296, 0);
            this.imageSlider1.Name = "imageSlider1";
            this.imageSlider1.Size = new System.Drawing.Size(234, 168);
            this.imageSlider1.TabIndex = 66;
            this.imageSlider1.Text = "imageSlider1";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(54, 133);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(38, 13);
            this.labelControl8.TabIndex = 75;
            this.labelControl8.Text = "Colour :";
            // 
            // textEdit18
            // 
            this.textEdit18.Location = new System.Drawing.Point(98, 130);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit18.Size = new System.Drawing.Size(193, 20);
            this.textEdit18.TabIndex = 76;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit4.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit4.Location = new System.Drawing.Point(370, 46);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit4.Properties.InitialImage")));
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit4.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit4.Size = new System.Drawing.Size(127, 104);
            this.pictureEdit4.TabIndex = 74;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit3.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit3.Location = new System.Drawing.Point(335, 33);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit3.Properties.InitialImage")));
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit3.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit3.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit3.TabIndex = 73;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit2.Location = new System.Drawing.Point(324, 18);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit2.Properties.InitialImage")));
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit2.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit2.TabIndex = 72;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::CarRentPro.Properties.Resources.no_photo_18;
            this.pictureEdit1.Location = new System.Drawing.Point(305, 14);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureEdit1.Properties.InitialImage")));
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEdit1.Size = new System.Drawing.Size(127, 105);
            this.pictureEdit1.TabIndex = 71;
            // 
            // dtpDelDate
            // 
            this.dtpDelDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDelDate.Location = new System.Drawing.Point(197, 153);
            this.dtpDelDate.Name = "dtpDelDate";
            this.dtpDelDate.Size = new System.Drawing.Size(93, 21);
            this.dtpDelDate.TabIndex = 70;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(98, 156);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(78, 13);
            this.labelControl9.TabIndex = 67;
            this.labelControl9.Text = "Delivered Date :";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(61, 59);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(31, 13);
            this.labelControl11.TabIndex = 68;
            this.labelControl11.Text = "Type :";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(72, 6);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(20, 13);
            this.labelControl10.TabIndex = 61;
            this.labelControl10.Text = "No :";
            // 
            // txtVhclNo
            // 
            this.txtVhclNo.Location = new System.Drawing.Point(97, 3);
            this.txtVhclNo.Name = "txtVhclNo";
            this.txtVhclNo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclNo.Size = new System.Drawing.Size(193, 20);
            this.txtVhclNo.TabIndex = 65;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(60, 33);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(32, 13);
            this.labelControl13.TabIndex = 60;
            this.labelControl13.Text = "Make :";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(57, 84);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(35, 13);
            this.labelControl14.TabIndex = 59;
            this.labelControl14.Text = "Model :";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(34, 110);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(58, 13);
            this.labelControl15.TabIndex = 58;
            this.labelControl15.Text = "Made Year :";
            // 
            // txtVhclMYeay
            // 
            this.txtVhclMYeay.Location = new System.Drawing.Point(98, 107);
            this.txtVhclMYeay.Name = "txtVhclMYeay";
            this.txtVhclMYeay.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclMYeay.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMYeay.TabIndex = 62;
            // 
            // txtVhclModel
            // 
            this.txtVhclModel.Location = new System.Drawing.Point(98, 81);
            this.txtVhclModel.Name = "txtVhclModel";
            this.txtVhclModel.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclModel.Size = new System.Drawing.Size(193, 20);
            this.txtVhclModel.TabIndex = 63;
            // 
            // txtVhclMake
            // 
            this.txtVhclMake.Location = new System.Drawing.Point(98, 30);
            this.txtVhclMake.Name = "txtVhclMake";
            this.txtVhclMake.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVhclMake.Size = new System.Drawing.Size(193, 20);
            this.txtVhclMake.TabIndex = 64;
            // 
            // txtVhclType
            // 
            this.txtVhclType.Location = new System.Drawing.Point(98, 56);
            this.txtVhclType.Name = "txtVhclType";
            this.txtVhclType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtVhclType.Properties.Items.AddRange(new object[] {
            "Car",
            "Van",
            "Bike"});
            this.txtVhclType.Size = new System.Drawing.Size(193, 20);
            this.txtVhclType.TabIndex = 69;
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(370, 134);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(62, 23);
            this.simpleButton8.TabIndex = 77;
            this.simpleButton8.Text = "Test";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.memoEdit1);
            this.xtraTabPage4.Controls.Add(this.dateTimePicker1);
            this.xtraTabPage4.Controls.Add(this.labelControl36);
            this.xtraTabPage4.Controls.Add(this.labelControl37);
            this.xtraTabPage4.Controls.Add(this.labelControl38);
            this.xtraTabPage4.Controls.Add(this.labelControl39);
            this.xtraTabPage4.Controls.Add(this.labelControl40);
            this.xtraTabPage4.Controls.Add(this.labelControl41);
            this.xtraTabPage4.Controls.Add(this.labelControl42);
            this.xtraTabPage4.Controls.Add(this.textEdit6);
            this.xtraTabPage4.Controls.Add(this.textEdit5);
            this.xtraTabPage4.Controls.Add(this.textEdit7);
            this.xtraTabPage4.Controls.Add(this.textEdit8);
            this.xtraTabPage4.Controls.Add(this.textEdit9);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(561, 348);
            this.xtraTabPage4.Text = "Driver Deatils";
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(154, 85);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.memoEdit1.Size = new System.Drawing.Size(304, 46);
            this.memoEdit1.TabIndex = 67;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(353, 188);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(105, 21);
            this.dateTimePicker1.TabIndex = 66;
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Appearance.Options.UseFont = true;
            this.labelControl36.Location = new System.Drawing.Point(378, 172);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(42, 14);
            this.labelControl36.TabIndex = 62;
            this.labelControl36.Text = "Ex: Date";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Appearance.Options.UseFont = true;
            this.labelControl37.Location = new System.Drawing.Point(74, 168);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(43, 14);
            this.labelControl37.TabIndex = 63;
            this.labelControl37.Text = "Contact :";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Appearance.Options.UseFont = true;
            this.labelControl38.Location = new System.Drawing.Point(74, 194);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(78, 14);
            this.labelControl38.TabIndex = 64;
            this.labelControl38.Text = "Driving  Lic No :";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Appearance.Options.UseFont = true;
            this.labelControl39.Location = new System.Drawing.Point(74, 144);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(23, 14);
            this.labelControl39.TabIndex = 61;
            this.labelControl39.Text = "NIC :";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Appearance.Options.UseFont = true;
            this.labelControl40.Location = new System.Drawing.Point(74, 223);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(70, 14);
            this.labelControl40.TabIndex = 60;
            this.labelControl40.Text = "Rste Per Day :";
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Appearance.Options.UseFont = true;
            this.labelControl41.Location = new System.Drawing.Point(74, 87);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(45, 14);
            this.labelControl41.TabIndex = 59;
            this.labelControl41.Text = "Address :";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Appearance.Options.UseFont = true;
            this.labelControl42.Location = new System.Drawing.Point(74, 61);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(36, 14);
            this.labelControl42.TabIndex = 58;
            this.labelControl42.Text = "Name :";
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(154, 137);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Size = new System.Drawing.Size(183, 20);
            this.textEdit6.TabIndex = 57;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(154, 220);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Size = new System.Drawing.Size(103, 20);
            this.textEdit5.TabIndex = 56;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(154, 191);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Size = new System.Drawing.Size(183, 20);
            this.textEdit7.TabIndex = 55;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(154, 165);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Size = new System.Drawing.Size(183, 20);
            this.textEdit8.TabIndex = 54;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(154, 58);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Size = new System.Drawing.Size(304, 20);
            this.textEdit9.TabIndex = 53;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.lstPayments);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(561, 348);
            this.xtraTabPage6.Text = "Payment deatils";
            // 
            // lstPayments
            // 
            this.lstPayments.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader16,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15});
            this.lstPayments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstPayments.FullRowSelect = true;
            this.lstPayments.Location = new System.Drawing.Point(0, 0);
            this.lstPayments.Name = "lstPayments";
            this.lstPayments.Size = new System.Drawing.Size(561, 348);
            this.lstPayments.TabIndex = 23;
            this.lstPayments.UseCompatibleStateImageBehavior = false;
            this.lstPayments.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Date";
            this.columnHeader16.Width = 150;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Pay Method";
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader12.Width = 100;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Card";
            this.columnHeader13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader13.Width = 75;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Card Ref";
            this.columnHeader14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader14.Width = 75;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Amount";
            this.columnHeader15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader15.Width = 150;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.checkEdit2);
            this.xtraTabPage3.Controls.Add(this.labelControl43);
            this.xtraTabPage3.Controls.Add(this.textEdit10);
            this.xtraTabPage3.Controls.Add(this.textEdit11);
            this.xtraTabPage3.Controls.Add(this.textEdit12);
            this.xtraTabPage3.Controls.Add(this.checkEdit3);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(561, 348);
            this.xtraTabPage3.Text = "Rate Deatils";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(237, 55);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Extra Charges (Rs.)";
            this.checkEdit2.Size = new System.Drawing.Size(123, 19);
            this.checkEdit2.TabIndex = 50;
            // 
            // labelControl43
            // 
            this.labelControl43.Location = new System.Drawing.Point(68, 31);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(89, 13);
            this.labelControl43.TabIndex = 49;
            this.labelControl43.Text = "Distance Limit (km)";
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(68, 50);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textEdit10.Size = new System.Drawing.Size(92, 20);
            this.textEdit10.TabIndex = 48;
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(385, 54);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textEdit11.Size = new System.Drawing.Size(92, 20);
            this.textEdit11.TabIndex = 47;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(384, 28);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textEdit12.Size = new System.Drawing.Size(93, 20);
            this.textEdit12.TabIndex = 46;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(237, 28);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Driver Charge Rate (Rs.)";
            this.checkEdit3.Size = new System.Drawing.Size(145, 19);
            this.checkEdit3.TabIndex = 45;
            // 
            // frmCheckOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 642);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.lblPaid);
            this.Controls.Add(this.lblDue);
            this.Controls.Add(this.labelControl29);
            this.Controls.Add(this.labelControl28);
            this.Controls.Add(this.labelControl25);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblGrandTotal);
            this.Controls.Add(this.btnPay);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.xtraTabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmCheckOut";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frmCheckOut_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtExKmCharge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShortTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtpetrolcharg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHrs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExCharges.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurODO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDriverList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbVehicleList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDisLimit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDriverCharges.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDriver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDayRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicCustomer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContact1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLIcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNIC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclLicNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclInsNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNxtSrvice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclCurOdo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMYeay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclModel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclMake.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclType.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit textEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl lblPaid;
        private DevExpress.XtraEditors.LabelControl lblDue;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.LabelControl lblGrandTotal;
        private DevExpress.XtraEditors.SimpleButton btnPay;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge cGauge1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent2;
        private DevExpress.XtraEditors.LabelControl lblPetrolValue;
        private DevExpress.XtraEditors.TextEdit txtpetrolcharg;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl45;
        private DevExpress.XtraEditors.TextEdit txtHrs;
        private DevExpress.XtraEditors.TextEdit txtDays;
        private DevExpress.XtraEditors.TextEdit txtExCharges;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit txtCurODO;
        private DevExpress.XtraEditors.TimeEdit tpEnd;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.TimeEdit tpStart;
        private DevExpress.XtraEditors.ComboBoxEdit cmbDriverList;
        private DevExpress.XtraEditors.ComboBoxEdit cmbVehicleList;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit txtDisLimit;
        private DevExpress.XtraEditors.TextEdit txtDriverCharges;
        private DevExpress.XtraEditors.CheckEdit chkDriver;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtDayRate;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.TextEdit txtContact3;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtContact2;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.SimpleButton AddCst;
        public DevExpress.XtraEditors.PictureEdit PicCustomer;
        private DevExpress.XtraEditors.LabelControl lblCstID;
        private DevExpress.XtraEditors.LabelControl lblIsBlackListed;
        private System.Windows.Forms.DateTimePicker dtpLicExp;
        private DevExpress.XtraEditors.TextEdit txtContact1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtLIcNo;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtAddress2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtAddress1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtFName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtNIC;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private System.Windows.Forms.ListView lstPayments;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.TextEdit txtShortTime;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtExKmCharge;
        private DevExpress.XtraEditors.Controls.ImageSlider imageSlider1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.DateTimePicker dtpDelDate;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txtVhclNo;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txtVhclMYeay;
        private DevExpress.XtraEditors.TextEdit txtVhclModel;
        private DevExpress.XtraEditors.TextEdit txtVhclMake;
        private DevExpress.XtraEditors.ComboBoxEdit txtVhclType;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.LabelControl lblVhclID;
        private System.Windows.Forms.DateTimePicker dtpLicExpDate;
        private System.Windows.Forms.DateTimePicker dtpInsExpDate;
        private DevExpress.XtraEditors.TextEdit txtVhclLicNo;
        private DevExpress.XtraEditors.TextEdit txtVhclInsNo;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txtVhclNxtSrvice;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtVhclCurOdo;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl lbldecor;
        private DevExpress.XtraEditors.LabelControl lblDriverCharge;
        private DevExpress.XtraEditors.LabelControl lblRental;

    }
}