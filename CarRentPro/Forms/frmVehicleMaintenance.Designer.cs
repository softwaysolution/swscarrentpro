﻿namespace CarRentPro.Forms
{
    partial class frmVehicleMaintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpServiced = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtGoilNext = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.txtMCharges = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtOilFilter = new DevExpress.XtraEditors.TextEdit();
            this.txtOtherFilter = new DevExpress.XtraEditors.TextEdit();
            this.txtGoilInterval = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.btnVhclSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtEoilIterval = new DevExpress.XtraEditors.TextEdit();
            this.txtEoilNext = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.txtEoilNote = new DevExpress.XtraEditors.TextEdit();
            this.txtGoilNote = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtCurrentODO = new DevExpress.XtraEditors.TextEdit();
            this.txtVhclNo = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpServiced.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpServiced.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoilNext.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMCharges.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOilFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoilInterval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEoilIterval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEoilNext.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEoilNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoilNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentODO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpServiced
            // 
            this.dtpServiced.EditValue = null;
            this.dtpServiced.Location = new System.Drawing.Point(127, 58);
            this.dtpServiced.Name = "dtpServiced";
            this.dtpServiced.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpServiced.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpServiced.Size = new System.Drawing.Size(215, 20);
            this.dtpServiced.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(59, 35);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(62, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "Vehicle No :  ";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(41, 61);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(80, 13);
            this.labelControl4.TabIndex = 9;
            this.labelControl4.Text = "Serviced Date :  ";
            // 
            // txtGoilNext
            // 
            this.txtGoilNext.Location = new System.Drawing.Point(223, 162);
            this.txtGoilNext.Name = "txtGoilNext";
            this.txtGoilNext.Size = new System.Drawing.Size(89, 20);
            this.txtGoilNext.TabIndex = 11;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(18, 161);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "ATF/CVT Oil";
            this.checkEdit2.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEdit2.Size = new System.Drawing.Size(86, 19);
            this.checkEdit2.TabIndex = 10;
            this.checkEdit2.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(18, 213);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Oil Filter";
            this.checkEdit3.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEdit3.Size = new System.Drawing.Size(86, 19);
            this.checkEdit3.TabIndex = 12;
            this.checkEdit3.CheckedChanged += new System.EventHandler(this.checkEdit3_CheckedChanged);
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(18, 239);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "Other Filters";
            this.checkEdit6.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEdit6.Size = new System.Drawing.Size(86, 19);
            this.checkEdit6.TabIndex = 18;
            this.checkEdit6.CheckedChanged += new System.EventHandler(this.checkEdit6_CheckedChanged);
            // 
            // txtMCharges
            // 
            this.txtMCharges.Location = new System.Drawing.Point(156, 316);
            this.txtMCharges.Name = "txtMCharges";
            this.txtMCharges.Size = new System.Drawing.Size(186, 20);
            this.txtMCharges.TabIndex = 33;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(59, 319);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(80, 13);
            this.labelControl5.TabIndex = 32;
            this.labelControl5.Text = "Service Charge :";
            // 
            // txtOilFilter
            // 
            this.txtOilFilter.Location = new System.Drawing.Point(127, 213);
            this.txtOilFilter.Name = "txtOilFilter";
            this.txtOilFilter.Size = new System.Drawing.Size(280, 20);
            this.txtOilFilter.TabIndex = 42;
            // 
            // txtOtherFilter
            // 
            this.txtOtherFilter.Location = new System.Drawing.Point(127, 239);
            this.txtOtherFilter.Name = "txtOtherFilter";
            this.txtOtherFilter.Size = new System.Drawing.Size(280, 20);
            this.txtOtherFilter.TabIndex = 36;
            // 
            // txtGoilInterval
            // 
            this.txtGoilInterval.Location = new System.Drawing.Point(127, 162);
            this.txtGoilInterval.Name = "txtGoilInterval";
            this.txtGoilInterval.Size = new System.Drawing.Size(90, 20);
            this.txtGoilInterval.TabIndex = 35;
            this.txtGoilInterval.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGoilInterval_KeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(137, 118);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(67, 13);
            this.labelControl6.TabIndex = 45;
            this.labelControl6.Text = "Serv. Interval";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(246, 118);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(23, 13);
            this.labelControl7.TabIndex = 46;
            this.labelControl7.Text = "Next";
            // 
            // btnVhclSave
            // 
            this.btnVhclSave.Location = new System.Drawing.Point(275, 357);
            this.btnVhclSave.Name = "btnVhclSave";
            this.btnVhclSave.Size = new System.Drawing.Size(75, 23);
            this.btnVhclSave.TabIndex = 47;
            this.btnVhclSave.Text = "Save";
            this.btnVhclSave.Click += new System.EventHandler(this.btnVhclSave_Click);
            // 
            // txtEoilIterval
            // 
            this.txtEoilIterval.Location = new System.Drawing.Point(127, 136);
            this.txtEoilIterval.Name = "txtEoilIterval";
            this.txtEoilIterval.Size = new System.Drawing.Size(90, 20);
            this.txtEoilIterval.TabIndex = 50;
            this.txtEoilIterval.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEoilIterval_KeyDown);
            // 
            // txtEoilNext
            // 
            this.txtEoilNext.Location = new System.Drawing.Point(223, 136);
            this.txtEoilNext.Name = "txtEoilNext";
            this.txtEoilNext.Size = new System.Drawing.Size(89, 20);
            this.txtEoilNext.TabIndex = 49;
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(18, 135);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "Engine Oil";
            this.checkEdit11.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.checkEdit11.Size = new System.Drawing.Size(86, 19);
            this.checkEdit11.TabIndex = 48;
            this.checkEdit11.CheckedChanged += new System.EventHandler(this.checkEdit11_CheckedChanged);
            // 
            // txtEoilNote
            // 
            this.txtEoilNote.Location = new System.Drawing.Point(318, 136);
            this.txtEoilNote.Name = "txtEoilNote";
            this.txtEoilNote.Size = new System.Drawing.Size(89, 20);
            this.txtEoilNote.TabIndex = 51;
            // 
            // txtGoilNote
            // 
            this.txtGoilNote.Location = new System.Drawing.Point(318, 162);
            this.txtGoilNote.Name = "txtGoilNote";
            this.txtGoilNote.Size = new System.Drawing.Size(89, 20);
            this.txtGoilNote.TabIndex = 52;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(352, 117);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(23, 13);
            this.labelControl8.TabIndex = 53;
            this.labelControl8.Text = "Note";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(45, 87);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 13);
            this.labelControl1.TabIndex = 55;
            this.labelControl1.Text = "Current ODO :  ";
            // 
            // txtCurrentODO
            // 
            this.txtCurrentODO.Location = new System.Drawing.Point(127, 84);
            this.txtCurrentODO.Name = "txtCurrentODO";
            this.txtCurrentODO.Size = new System.Drawing.Size(215, 20);
            this.txtCurrentODO.TabIndex = 54;
            // 
            // txtVhclNo
            // 
            this.txtVhclNo.Location = new System.Drawing.Point(127, 32);
            this.txtVhclNo.Name = "txtVhclNo";
            this.txtVhclNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtVhclNo.Size = new System.Drawing.Size(215, 20);
            this.txtVhclNo.TabIndex = 2;
            // 
            // frmVehicleMaintenance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 389);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtCurrentODO);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.txtGoilNote);
            this.Controls.Add(this.txtEoilNote);
            this.Controls.Add(this.txtEoilIterval);
            this.Controls.Add(this.txtEoilNext);
            this.Controls.Add(this.checkEdit11);
            this.Controls.Add(this.btnVhclSave);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.txtOilFilter);
            this.Controls.Add(this.txtOtherFilter);
            this.Controls.Add(this.txtGoilInterval);
            this.Controls.Add(this.txtMCharges);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.checkEdit6);
            this.Controls.Add(this.checkEdit3);
            this.Controls.Add(this.txtGoilNext);
            this.Controls.Add(this.checkEdit2);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.dtpServiced);
            this.Controls.Add(this.txtVhclNo);
            this.Name = "frmVehicleMaintenance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maintenance";
            this.Load += new System.EventHandler(this.frmVehicleMaintenance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtpServiced.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpServiced.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoilNext.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMCharges.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOilFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOtherFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoilInterval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEoilIterval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEoilNext.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEoilNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGoilNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCurrentODO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVhclNo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.DateEdit dtpServiced;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtGoilNext;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.TextEdit txtMCharges;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtOilFilter;
        private DevExpress.XtraEditors.TextEdit txtOtherFilter;
        private DevExpress.XtraEditors.TextEdit txtGoilInterval;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton btnVhclSave;
        private DevExpress.XtraEditors.TextEdit txtEoilIterval;
        private DevExpress.XtraEditors.TextEdit txtEoilNext;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.TextEdit txtEoilNote;
        private DevExpress.XtraEditors.TextEdit txtGoilNote;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtCurrentODO;
        private DevExpress.XtraEditors.ComboBoxEdit txtVhclNo;
    }
}