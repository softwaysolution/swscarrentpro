﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwaysolutionSys.Entities
{
    class Supplier
    {
        private string _supID;
        private string _supName;
        private string _supNIC;
        private string _supAddress1;
        private string _supTel;
        private string _supAddress2;

        public string SupAddress2
        {
            get { return _supAddress2; }
            set { _supAddress2 = value; }
        }

        public string SupTel
        {
            get { return _supTel; }
            set { _supTel = value; }
        }

        public string SupAddress
        {
            get { return _supAddress1; }
            set { _supAddress1 = value; }
        }

        public string SupNIC
        {
            get { return _supNIC; }
            set { _supNIC = value; }
        }

        public string SupName
        {
            get { return _supName; }
            set { _supName = value; }
        }

        public string SupID
        {
            get { return _supID; }
            set { _supID = value; }
        }
    }
}
