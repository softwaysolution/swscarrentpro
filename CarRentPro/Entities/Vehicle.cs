﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwaysolutionSys.Entities
{
    class Vehicle
    {
        private string _vhclID;
        private string _vhclNo;
        private string _vhclType;
        private string _vhclClass;
        private string _vhclSize;
        private string _vhclModel;
        private double _odoReading;
        private DateTime _LicExpDate;
        private double _NextService;
        private string _VhclOwner;
        private string _VhclOwnerID;

        public string VhclID
        {
            get { return _vhclID; }
            set { _vhclID = value; }
        }

        public string VhclNo
        {
            get { return _vhclNo; }
            set { _vhclNo = value; }
        }

        public string VhclType
        {
            get { return _vhclType; }
            set { _vhclType = value; }
        }

        public string VhclClass
        {
            get { return _vhclClass; }
            set { _vhclClass = value; }
        }

        public string VhclSize
        {
            get { return _vhclSize; }
            set { _vhclSize = value; }
        }

        public string VhclModel
        {
            get { return _vhclModel; }
            set { _vhclModel = value; }
        }

        public double OdoReading
        {
            get { return _odoReading; }
            set { _odoReading = value; }
        }

        public double NextService
        {
            get { return _NextService; }
            set { _NextService = value; }
        }

        public DateTime LicExpDate
        {
            get { return _LicExpDate; }
            set { _LicExpDate = value; }
        }

        public string VhclOwner
        {
            get { return _VhclOwner; }
            set { _VhclOwner = value; }
        }

        public string VhclOwnerID
        {
            get { return _VhclOwnerID; }
            set { _VhclOwnerID = value; }
        }
    }
}
