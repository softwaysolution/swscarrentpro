﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwaysolutionSys.Entities
{
    class Rental
    {
        private string _rntlID;
        private string _rntlCstID;
        private string _rntlVhclID;
        private DateTime _rntlPickupDate;
        private DateTime _rntlReturnDate;
        private int _rntlFuelVol;
        private double _rntlodoreading;
        private double _rntlRate;
        private double _rntlDecCharge;
        private bool _withDriver;
        private double _driverCharge;

        public double DriverCharge
        {
            get { return _driverCharge; }
            set { _driverCharge = value; }
        }

        public bool WithDriver
        {
            get { return _withDriver; }
            set { _withDriver = value; }
        }

        public double RntlDecCharge
        {
            get { return _rntlDecCharge; }
            set { _rntlDecCharge = value; }
        }

        public double RntlRate
        {
            get { return _rntlRate; }
            set { _rntlRate = value; }
        }

        public string RntlID
        {
            get { return _rntlID; }
            set { _rntlID = value; }
        }

        public string RntlCstID
        {
            get { return _rntlCstID; }
            set { _rntlCstID = value; }
        }

        public string RntlVhclID
        {
            get { return _rntlVhclID; }
            set { _rntlVhclID = value; }
        }

        public DateTime RntlPickupDate
        {
            get { return _rntlPickupDate; }
            set { _rntlPickupDate = value; }
        }

        public DateTime RntlReturnDate
        {
            get { return _rntlReturnDate; }
            set { _rntlReturnDate = value; }
        }

        public int RntlFuelVol
        {
            get { return _rntlFuelVol; }
            set { _rntlFuelVol = value; }
        }

        public double Rntlodoreading
        {
            get { return _rntlodoreading; }
            set { _rntlodoreading = value; }
        }
    }
}
