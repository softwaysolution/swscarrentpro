﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentPro
{
    public class PayDoneEventArgs : EventArgs
    {
        private string _payMethod;
        private string _cardName;
        private string _cardRef;
        private double _payment;
        private DateTime _payTime;

        public DateTime PayTime
        {
            get { return _payTime; }
            set { _payTime = value; }
        }
        public double Payment
        {
            get { return _payment; }
            set { _payment = value; }
        }
        public string CardRef
        {
            get { return _cardRef; }
            set { _cardRef = value; }
        }
        public string CardName
        {
            get { return _cardName; }
            set { _cardName = value; }
        }
        public string PayMethod
        {
            get { return _payMethod; }
            set { _payMethod = value; }
        }

        public PayDoneEventArgs(string payMeth, string crdName, string crdRef, double payment, DateTime payTime)
        {
            this.PayMethod = payMeth;
            this.CardName = crdName;
            this.CardRef = crdRef;
            this.Payment = payment;
            this.PayTime = payTime;
        }
    }
}
